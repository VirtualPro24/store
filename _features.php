<div class="features"> 
    <div class="container"> 
        <div class="list">
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/1.svg" alt=""></div>
                <p>Безкоштовна доставка</p>
            </div>
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/2.svg" alt=""></div>
                <p>Знижки постiйним клiєнтам</p>
            </div>
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/3.svg" alt=""></div>
                <p>30 днів на повернення</p>
            </div>
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/4.svg" alt=""></div>
                <p>Допомога у виборі розміру</p>
            </div>
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/5.svg" alt=""></div>
                <p>100% Гарантія якості</p>
            </div>
            <div class="item">
                <div class="icon"><img class="lazy" data-src="img/features/6.svg" alt=""></div>
                <p>Оплата при отриманні</p>
            </div>                                                                               
        </div>
    </div> 
</div>