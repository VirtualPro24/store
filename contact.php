<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Контакти</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block" class="bg-2">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="simple-page ">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Контакти</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="title-page h2">Контакти</div>

                <div class="contact-block">
                    <div class="contact-item">
                        <div class="map-wrap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2572.9060134846904!2d24.021911315709527!3d49.84422347939642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473add730c752709%3A0xc8c7f4cc48a1340c!2sMAGNUS!5e0!3m2!1suk!2sua!4v1622614621459!5m2!1suk!2sua" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                        <div class="info">
                            <div class="block-title type-2">FAVORITE SHOES МАГНУС</div>
                            <div class="work-time">ПН-НД: 10:00-20:00</div>
                            <div class="location">м. Львів вул. Шпитальна, 1
                                ТЦ "Магнус" 2-й поверх </div>
                        </div>
                        <div class="image" style="background-image:url(img/contacts/image-type-1.jpg)">
                        </div>
                    </div>
                    <div class="contact-item">
                        <div class="map-wrap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2572.9060134846904!2d24.021911315709527!3d49.84422347939642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473add730c752709%3A0xc8c7f4cc48a1340c!2sMAGNUS!5e0!3m2!1suk!2sua!4v1622614621459!5m2!1suk!2sua" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                        <div class="info">
                            <div class="block-title type-2">LEBERDES БРЕНДОВИЙ
                                МАГАЗИН</div>
                            <div class="work-time">ПН-НД: 10:00-20:00</div>
                            <div class="location">м. Львів вул. Виговського , 100
                                ТЦ "ВАМ" 2 поверх </div>
                        </div>
                        <div class="image" style="background-image:url(img/contacts/image-type-1.jpg)">
                        </div>
                    </div>
                    <div class="contact-item w100">
                        <div class="map-wrap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2572.9060134846904!2d24.021911315709527!3d49.84422347939642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473add730c752709%3A0xc8c7f4cc48a1340c!2sMAGNUS!5e0!3m2!1suk!2sua!4v1622614621459!5m2!1suk!2sua" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                        <div class="info">
                            <div class="block-title type-2">FAVORITE SHOES ПІВДЕННИЙ</div>
                            <div class="work-time">ПН-НД: 10:00-20:00</div>
                            <div class="location">ТВК "Південний" Торгівельна лінія "Володимирська"</div>
                        </div>
                        <div class="image" style="background-image:url(img/contacts/image-type-2.jpg)">
                        </div>
                    </div>
                </div>

                <div class="contact-form">
                    <div class="cf-text">
                        <div class="cf-title">Зворотній зв'язок </div>
                        <p>Ми раді відповісти на Ваші питання. Введіть адресу електроної пошти, на яку ми пришлемо відповідь на Ваше питання. Постараємося відповісти максимально швидко.</p>
                    </div>
                        <form action="">
                            <div class="input-wrap type-2 with-icon wrap-white">
                                <img src="img/user.svg" alt="" class="icon">
                                <label for="t2" class="input-title">Iм’я</label>
                                <input type="text" id="t2" class="input">
                            </div>
                            <div class="input-wrap type-2 with-icon wrap-white w50">
                                <img src="img/icon-email.svg" alt="" class="icon">
                                <label for="t1" class="input-title">E-mail</label>
                                <input type="email" id="t1" class="input">
                            </div>
                            <div class="input-wrap type-2 with-icon wrap-white w50">
                                <img src="img/phone-brown.svg" alt="" class="icon">
                                <label for="t3" class="input-title">Телефон</label>
                                <input type="tel" id="t3" class="input">
                            </div>                            
                            <div class="add-comment-block type-2" style="display:block;">

                                <div class="input-wrap type-2">
                                    <textarea name="" maxlength="2000" class="input type-2" id="comment1" placeholder="Сообщение"></textarea>
                                </div>
                                <p><span class="count-area">0</span>/<span class="max-count">2000</span></p>
                            </div>
                            <button class="btn btn-primary" disabled>Відтправити</button>
                        </form>                    
                </div>
            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
