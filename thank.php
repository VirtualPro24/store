<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Дякуємо за покупки!</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="header">
            <? include '_header-order.php';?>
        </header>


        <div class="thank">
            <div class="container">
                <div class="page-header">
                    <a href="categories.php" class="link-back">Назад до покупок</a>
                </div>
            </div>
            <div class="thank-page">
                <div class="container">
                    <div class="thank-header">
                        <img src="img/thank-image.jpg" alt="">
                        <div class="title">Дякуємо за покупки!</div>
                        <p>Наш консультант зв'яжеться з вами наближчим часом для підтвердження деталей</p>
                    </div>

                    <div class="thank-row">
                        <div class="thank-col">
                            <div class="block-title type-2">Ви замовили:</div>
                            <div class="order-list mc-table">
                                <div class="mc-tr" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/cart/cart.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                            <div class="size">
                                                Розмір <span>35</span>
                                            </div>
                                        </div>
                                        <div class="price">2789 ₴</div>

                                    </div>
                                </div>
                                <div class="mc-tr" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/color-2.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                            <div class="size">
                                                Розмір <span>35</span>
                                            </div>
                                        </div>
                                        <div class="price">2789 ₴</div>

                                    </div>
                                </div>
                                <div class="mc-tr" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/other-prod.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>

                                        </div>
                                        <div class="price">2789 ₴</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thank-col">
                            <div class="block-title type-2">Деталі замовлення</div>
                            <div class="order-detail">
                                <div class="order-table">
                                    <div class="order-tr">
                                        <div class="order-td">Замовлення</div>
                                        <div class="order-td">№ 9074634</div>
                                    </div>
                                    <div class="order-tr">
                                        <div class="order-td">Зроблено</div>
                                        <div class="order-td">20 Марта 2020</div>
                                    </div>
                                    <div class="order-tr">
                                        <div class="order-td">Имя</div>
                                        <div class="order-td">Иванов Иван</div>
                                    </div>
                                    <div class="order-tr">
                                        <div class="order-td">Телефон</div>
                                        <div class="order-td">+38 (096) 123 45 67</div>
                                    </div>
                                    <div class="order-tr">
                                        <div class="order-td">Доставка</div>
                                        <div class="order-td">Нова Пошта,
                                            м.Хмельницький, відділення №27</div>
                                    </div>
                                </div>
                                <a href="" class="btn btn-stroke type-2 back-to-shop">
                                    перейти в магазин
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="thank-bottom">
                        <div class="thank-bottom-header">
                            <div class="block-title type-2">Реєструйтесь на сайті, щоб:</div>
                            <a href="" class="link">Зарегеструватись</a>
                        </div>
                        <div class="thank-bottom-list">
                            <div class="item">
                                <div class="icon"></div>
                                <p>Накопичувати бали
                                    з кожної</p>
                            </div>
                            <div class="item">
                                <div class="icon"></div>
                                <p>Відчути переваги
                                    швидкого замовлення</p>
                            </div>
                            <div class="item">
                                <div class="icon"></div>
                                <p>Слідкувати за історією
                                    власних покупок</p>
                            </div>
                            <div class="item">
                                <div class="icon"></div>
                                <p>Бути вкурсі акційних пропозицій новин</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>



    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
