<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Зарегеструватись</title>
</head>

<body class="home-page">

    <div id="content-block" class="bg-1">

        <div class="sign page">
            <div class="sign-header">
                <a href="categories.php" class="link-back">Назад в магазин</a>
                <a href="" class="logo">
                    <img src="img/logo.svg" alt="">
                </a>
            </div>
            <div class="sign-page-content">
                <div class="sign-row">
                    <div class="sign-image sign-col">
                        <img src="img/sign/sign-img.jpg" alt="">
                    </div>
                    <div class="sign-col sign-content bg-2">
                        <div class="title h2 type-2">Зареєструватись</div>
                        <div class="sign-social">
                            <div class="subtitle small-13 fw-500">За допомогою соцмереж</div>
                            <div class="btn-group">
                                <a href="" class="btn btn-stroke type-2 btn-fb">
                                    Facebook
                                </a>
                                <a href="" class="btn btn-stroke type-2 btn-google">
                                    Google
                                </a>
                            </div>
                        </div>
                        <div class="sign-form">
                            <div class="subtitle small-13 fw-500">Через E-mail</div>
                            <form action="">
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/icon-email.svg" alt="" class="icon">
                                    <label for="t1" class="input-title">E-mail</label>
                                    <input type="email" id="t1" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t2" class="input-title">Iм’я</label>
                                    <input type="text" id="t2" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t3" class="input-title">Прізвище</label>
                                    <input type="text" id="t3" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/sign/icon-lock.svg" alt="" class="icon">
                                    <label for="t4" class="input-title">Пароль</label>
                                    <input type="password" id="t4" class="input">
                                    <span class="icon-change-view" data-show-password></span>
                                </div>
                                <button class="btn btn-primary disabled" disabled>Зареєструватись</button>

                            </form>
                            <div class="sign-form-info">
                                <div class="small-13 type-2">Маєте аккаунт ?</div>
                                <a href="login.php" class="link type-2">Увiйти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
