<div class="header-order hidden-xs">
    <div class="container">
        <div class="left">
             <div class="help type-2">
                <a href="" class="help-link">Допомога</a>
                <div class="help-list">
                    <ul>
                        <li><a href="" class="link">Покупцю</a></li>
                        <li><a href="" class="link">Оплата та доставка</a></li>
                        <li><a href="" class="link">Дисконтна пограма</a></li>
                        <li><a href="" class="link">Блог</a></li>
                        <li><a href="contact.php" class="link">Контакти</a></li>
                    </ul>
                </div>
            </div>
            <div class="phones callback-wrap">
                <a href="tel:">0 800 216 959</a>
                <div class="arrow"><img src="img/arrow-down.svg" alt=""></div>
                <div class="contact-info">
                    <div class="phone-list">
                        <ul>
                            <li><a href="tel:0800216959">0 800 216 959</a></li>
                            <li>
                                <a href="tel:0800216959">096 991 15 90</a>
                            </li>
                            <li><a href="tel:0800216959">063 178 12 27</a></li>
                            <li><a href="tel:0800216959">050 200 77 79</a></li>
                            <li><a href="tel:0800216959">050 105 80 17</a></li>
                        </ul>
                    </div>

                    <div class="work-time">
                        <div class="work-time-title">Режим роботи</div>
                        <ul>
                            <li><span>ПН-ПТ:</span> 10:00 — 19:00</li>
                            <li><span>CБ:</span> 11:00 — 18:00</li>
                            <li><span>НД:</span> ВИХІДНИЙ</li>
                        </ul>
                    </div>

                    <div class="form-callback">
                        <form action="">
                            <label>Введіть Ваш номер телефону</label>
                            <input type="tel" class="form-control" placeholder="+380 (96) 123 45 67">
                            <button class="btn btn-primary type-2 w100">Зателефонуйте мені</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <a href="" class="logo">
            <img src="img/logo.svg" alt="">
        </a>
        <div class="lang">
            <ul>
                <li class="active"><a href="">UA</a></li>
                <li><a href="">RU</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="header-top only-mob">
    <div class="container">
        <a href="" class="logo">
            <img src="img/logo.svg" alt="">
        </a>
    </div>
</div>

<div class="header-inner only-mob">
    <div class="container">
        <div class="mobile-button"><span></span></div>


        <div class="header-options">
            <div class="form-search">
                <div class="icon"><img src="img/icon-search.svg" alt=""></div>
                <div class="search-wrap">
                    <form action="">
                        <input type="text" placeholder="Пошук">
                        <button type="submit"></button>
                    </form>
                    <div class="clear-search"></div>
                    <div class="form-search-list">
                        <ul>
                            <li><a href="search.php">Жіночі кросівки</a></li>
                            <li><a href="search.php">Кросівки жіночі</a></li>
                            <li><a href="search.php">Кросівки</a></li>
                            <li><a href="search.php">Кросівки Bardes</a></li>
                            <li><a href="search.php">Кросівки Bardes</a></li>
                        </ul>
                    </div>
                    <div class="form-search-close"></div>
                </div>
            </div>
            <div class="header-options-list">
                <ul>
                    <li>
                        <a href="">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.9996 21.0543C-8 10.0002 5.99999 -1.99984 11.9996 5.58821C18 -1.99985 32 10.0002 11.9996 21.0543Z" />
                            </svg>
                        </a>
                    </li>
                    <li class="cabinet-link">
                        <a href="">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <ellipse cx="12" cy="6.5" rx="4" ry="4.5" />
                                <path d="M20 19C20 22.5 16 21.5 12 21.5C8 21.5 4 22.5 4 19C4 17 7.58172 14.5 12 14.5C16.4183 14.5 20 17 20 19Z" />
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href="" class="active">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.5 7C8.5 5.067 10.067 3.5 12 3.5C13.933 3.5 15.5 5.067 15.5 7V10.5H8.5V7ZM16.5 7V10.5H19C19.2383 10.5 19.4436 10.6682 19.4903 10.9019L21.4903 20.9019C21.5197 21.0488 21.4816 21.2012 21.3867 21.317C21.2917 21.4328 21.1498 21.5 21 21.5H3C2.85021 21.5 2.7083 21.4328 2.61333 21.317C2.51836 21.2012 2.48034 21.0488 2.50971 20.9019L4.50971 10.9019C4.55646 10.6682 4.76166 10.5 5 10.5H7.5V7C7.5 4.51472 9.51472 2.5 12 2.5C14.4853 2.5 16.5 4.51472 16.5 7Z" fill="black" />
                            </svg>

                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>


<div class="mobile-menu">
    <div class="mob-nav">
        <ul>
            <li><a href="" class="active" data-mob-nav="nav-1">Жіноче</a></li>
            <li><a href="" data-mob-nav="nav-2">Чоловіче</a></li>
        </ul>
        <div class="item-nav nav-1 active">
            <!--            <a href="" class="back">Назад</a>-->
            <!--            <div class="title">Жіноче</div>-->
            <ul>
                <li><a href="" data-subnav="subnav-1">Взуття</a></li>
                <li><a href="">Знижки</a></li>
                <li><a href="">Аксесуари</a></li>
                <li><a href="">Догляд</a></li>
                <li><a href="">Акцiї</a></li>
            </ul>
        </div>
        <div class="item-nav nav-2">
            <!--            <a href="" class="back">Назад</a>-->
            <!--            <div class="title">Чоловіче</div>-->
            <ul>
                <li><a href="" data-subnav="subnav-2">Взуття</a></li>
                <li><a href="">Знижки</a></li>
                <li><a href="">Аксесуари</a></li>
                <li><a href="">Догляд</a></li>
                <li><a href="">Акцiї</a></li>
            </ul>
        </div>
        <a href="">Допомога</a>
    </div>
    <div class="wrap-item-nav">
        <div class="item-subnav subnav-1">
            <a href="" class="back">Назад</a>
            <div class="title">Жіноче / взуття</div>

            <ul>
                <li>
                    <a href="" class="submenu-title">Туфлі</a>
                    <ol>
                        <li class="underline"><a href="">дивитись всi</a></li>
                        <li><a href="">Туфлі на каблуку</a></li>
                        <li><a href="">Туфлі на грубому каблуку</a></li>
                        <li><a href="">Туфлі-човники</a></li>
                        <li><a href="">Дербі жіночі</a></li>
                        <li><a href="">Лофери жіночі</a></li>
                        <li><a href="">Вечірні туфлі</a></li>
                        <li><a href="">Туфлі на низькому ходу</a></li>
                    </ol>
                </li>
                <li>
                    <a href="" class="submenu-title">Кросівки та кеди</a>
                    <ol>
                        <li><a href="">Кросівки</a></li>
                        <li><a href="">Кеди</a></li>
                        <li><a href="">Снікерси</a></li>
                    </ol>
                </li>
                <li>
                    <a href="" class="submenu-title">Ботільйони</a>
                    <ol>
                        <li><a href="">Ковбойки</a></li>
                    </ol>
                </li>
                <li>
                    <a href="" class="submenu-title">БоСонiжки</a>
                    <ol>
                        <li><a href="">Слінгбеки</a></li>
                        <li><a href="">Босоніжки на платформі</a></li>
                    </ol>
                </li>
                <li><a href="">Пляжні шльопанці</a></li>
                <li><a href="">угi</a></li>
                <li><a href="">made in italy</a></li>
                <li><a href="">Мокасини</a></li>
                <li>
                    <a href="" class="submenu-title">черевики</a>
                    <ol>
                        <li><a href="">Черевики осінні</a></li>
                        <li><a href="">Черевики зимові</a></li>
                        <li><a href="">Челсі жіночі</a></li>
                        <li><a href="">Дутiки</a></li>
                        <li><a href="">Хайтопи</a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </div>

    <div class="modile-option">
        <div class="lang">
            <ul>
                <li class="active"><a href="">UA</a></li>
                <li><a href="">RU</a></li>
            </ul>
        </div>
        <a href="" class="btn btn-stroke type-2">Особистий кабінет</a>
    </div>

    <div class="phone-list">
        <div class="phone-toggle">
            <a href="">0 800 216 959</a>
        </div>
        <ul>
            <li><a href="tel:0800216959">0 800 216 959</a></li>
            <li>
                <a href="tel:0800216959">096 991 15 90</a>
            </li>
            <li><a href="tel:0800216959">063 178 12 27</a></li>
            <li><a href="tel:0800216959">050 200 77 79</a></li>
            <li><a href="tel:0800216959">050 105 80 17</a></li>
        </ul>
    </div>

    <div class="work-time">
        <div class="work-time-title">Режим роботи</div>
        <ul>
            <li><span>ПН-ПТ:</span> 10:00 — 19:00</li>
            <li><span>CБ:</span> 11:00 — 18:00</li>
            <li><span>НД:</span> ВИХІДНИЙ</li>
        </ul>
    </div>

    <div class="form-callback">
        <form action="">
            <label>Введіть Ваш номер телефону</label>
            <input type="tel" class="form-control" placeholder="+380 (96) 123 45 67">
            <button class="btn btn-primary type-2 w100">Зателефонуйте мені</button>
        </form>
    </div>
</div>
