<link href="css/sumoselect.css" rel="stylesheet">
<link href="css/swiper.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />


<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/jquery.sumoselect.min.js"></script>
<script src="js/jquery.inputmask.min.js"></script>
<script src="js/SmoothScroll.js"></script>
<script src="js/lazyload.min.js"></script>
<script src="js/main.js"></script>


<script>
    setTimeout(function() {

        var lazyLoadInstance = new LazyLoad({
            elements_selector: ".lazy"
        });
    }, 500)
</script>
