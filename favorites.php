<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Обране</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block" class="bg-2">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="favorites">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Обране</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="title-page h2">Обране</div>

                <div class="fv-list product-list">
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                            
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                            
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge new">New</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-3.jpg" alt="">
                            </a>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge new">New</div>
                            </div>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                    <div class="product" data-popover-wrap itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active" data-open-popover></a>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/fv-3.jpg" alt="">
                            </a>
                            <div class="favorite-popover popover">
                                <p>Видалити товар з обраного?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-favorite-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link" data-popover-close>Відмінити</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>                             
                        </div>
                    </div>
                </div>
                
                <!-- PAGINATION -->
                <section class="list-show-options"> 
                    <? include '_pagination.php';?>
                </section>                
            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
