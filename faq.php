<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | FAQ</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block" class="bg-2">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="faq-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">FAQ</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="title-page h2">FAQ</div>
                <div class="faq-block">
                    <div class="faq-content">
                        <div class="faq-row">
                            <div class="faq-row-title h2">Покупки</div>
                            <div class="faq-row-list">
                                <div class="item">
                                    <div class="text-15 fw-500">Могу ли я сохранить избранные товары, чтобы заказать их позже?</div>
                                    <div class="fi-content">
                                        <p>Да, зарегистрированные пользователи могут сохранять понравившиеся товары в разделе <a href=""> Избранные товары</a>.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Отличаются ли цены в интернет-магазине MD Fashion от цен в магазинах-партнерах сети?</div>
                                    <div class="fi-content">
                                        <p>Нет. Цены в интернет-магазине MD Fashion и магазинах-партнерах сети одинаковы. Исключением могут стать наличие специальных акций и дисконтная программа для зарегистрированных пользователей сайта.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Могу ли я примерить товар?</div>
                                    <div class="fi-content">
                                        <p>При заказах с доставкой по адресу или до отделений “Нова пошта” и “Justin” услуга примерки сейчас недоступна.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="faq-row">
                            <div class="faq-row-title h2">Возврат</div>
                            <div class="faq-row-list">
                                <div class="item">
                                    <div class="text-15 fw-500">Могу ли я вернуть товар, который мне не подошел по размеру, цвету, фасону?</div>
                                    <div class="fi-content">
                                        <p>Вы можете вернуть товар, который по каким-либо причинам вам не подошел, при условии сохранения его товарного вида, упаковки, этикеток и товарной накладной. Поэтому мы рекомендуем примерить и внимательно осмотреть товар, когда вы его получите, в комфортной обстановке, не срывая этикеток. На возврат товара у вас будет 14 дней с момента покупки. Более детально со списком товаров, которые не подлежат возврату, вы можете ознакомиться по ссылке.
                                            Обратите внимание: при некоторых акциях и специальных предложениях, возможен возврат только всех товаров из заказа.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Кто оплачивает пересылку товара при возврате?</div>
                                    <div class="fi-content">
                                        <p>Затраты по отправке товара при возврате мы берем на себя. Вам не нужно ничего оплачивать вне зависимости от способа передачи товара. При отправке посылки при помощи "Нова пошта", "Укрпошта" или “Justin” просто выберите оплату получателем.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">В какие сроки я могу вернуть товар?</div>
                                    <div class="fi-content">
                                        <p>Затраты по отправке товара при возврате мы берем на себя. Вам не нужно ничего оплачивать вне зависимости от способа передачи товара. При отправке посылки при помощи "Нова пошта", "Укрпошта" или “Justin” просто выберите оплату получателем.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Кто оплачивает пересылку товара при возврате?</div>
                                    <div class="fi-content">
                                        <p>Затраты по отправке товара при возврате мы берем на себя. Вам не нужно ничего оплачивать вне зависимости от способа передачи товара. При отправке посылки при помощи "Нова пошта", "Укрпошта" или “Justin” просто выберите оплату получателем.</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Кто оплачивает пересылку товара при возврате?</div>
                                    <div class="fi-content">
                                        <p>Какие документы необходимы, для осуществления возврата товара?</p>
                                        <ul>
                                            <li><a href="">Заявление на возврат через отделение “Нова пошта”</a></li>
                                            <li><a href="">Заявление на возврат через отделение “Justin”</a></li>
                                            <li><a href="">Заявление на возврат через отделение “Укрпошта”</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="text-15 fw-500">Кто оплачивает пересылку товара при возврате?</div>
                                    <div class="fi-content">
                                        <p>Как предусмотрено законодательством Украины, вы можете вернуть товар в течение 14 дней с момента покупки.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-form">
                        <div class="text-15 fw-500">Не знайшли відповідь
                            на ваше питання, напишіть нам</div>
                        <form action="">
                            <div class="input-wrap type-2 with-icon wrap-white">
                                <img src="img/user.svg" alt="" class="icon">
                                <label for="t2" class="input-title">Iм’я</label>
                                <input type="text" id="t2" class="input">
                            </div>
                            <div class="input-wrap type-2 with-icon wrap-white">
                                <img src="img/icon-email.svg" alt="" class="icon">
                                <label for="t1" class="input-title">E-mail</label>
                                <input type="email" id="t1" class="input">
                            </div>
                            <div class="add-comment-block" style="display:block;">

                                <div class="input-wrap type-2">
                                    <textarea name="" maxlength="2000" class="input type-2" id="comment1" placeholder="Ваше запитання"></textarea>
                                </div>
                                <p><span class="count-area">0</span>/<span class="max-count">2000</span></p>
                            </div>
                            <button class="btn btn-primary">Відтправити</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
