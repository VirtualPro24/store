<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Новини та акції</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page blog-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Оплата та доставка</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="h1 page-title fw-500">Новини та акції</div>

                 <div class="blog-row">
                     <div class="col-4">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-1.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">LOVE IS... -15% на всі аксесуари</a>
                             <div class="text-15">До Дня Закоханих</div>
                         </div>
                     </div>
                     <div class="col-8">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-2.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">Які кросівки купити на літо 2021?</a>
                             <div class="text-15">Розбираємося у всіх тонкощах вибору</div>
                         </div>
                     </div>   
                     <div class="col-4">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-3.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">LOVE IS... -15% на всі аксесуари</a>
                             <div class="text-15">До Дня Закоханих</div>
                         </div>
                     </div>   
                     <div class="col-4">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-4.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">НІЧ ЕКСТРА ЗНИЖОК -15% додатково на зимове взуття</a>
                             <div class="text-15">До Дня Закоханих</div>
                         </div>
                     </div>   
                     <div class="col-4">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-3.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">Графік роботи на свята Favorite Shoes</a>
                             <div class="text-15">До Дня Закоханих</div>
                         </div>
                     </div>
                     <div class="col-8">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-2.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">Які кросівки купити на літо 2021?</a>
                             <div class="text-15">Розбираємося у всіх тонкощах вибору</div>
                         </div>
                     </div>                       
                     <div class="col-4">
                         <div class="blog-item">
                             <a href="" class="image" style="background-image:url(img/news/image-1.jpg)"> 
                                 <div class="date">18/01/2021</div>
                             </a>
                             <a href="" class="bi-title fw-500">LOVE IS... -15% на всі аксесуари</a>
                             <div class="text-15">До Дня Закоханих</div>
                         </div>
                     </div>
                 </div>
                 
                <section class="list-show-options"> 
                    <? include '_pagination.php';?>

                </section>                 
            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
