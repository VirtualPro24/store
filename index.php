<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Home</title>
</head>

<body class="home-page">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>

        <!-- MAIN -->
        <section class="main">
            <div class="container">
                <div class="swiper-entry custom-fraction-swiper">
                    <div class="swiper-container" data-options='{"slidesPerView":1,"spaceBetween":50, "customFraction":true }'>
                        <div class="swiper-button-group">
                            <div class="swiper-button-prev button"><img src="img/slide-arrow-left.svg" alt=""></div>
                            <div class="swiper-button-next button"><img src="img/slide-arrow-right.svg" alt=""></div>
                        </div>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="lazy" src="data:image/gif;base64,R0lGODlh+AQ1AoAAAP///wAAACH5BAEAAAEALAAAAAD4BDUCAAL+jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8b+jRw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP778+fTr27+PP7/+/fz++/v/D2CAAg5IYIEGHohgggouyGCDDj4IYYQSTkhhhRZeiGGGGm7IYYcefghiiCKOSGKJJp6IYooqrshiiy6+CGOMMs5IY4023ohjjjruyGOPPv4IZJBCDklkkUYeiWSSSi7JZJNOPglllFJOSWWVVl6JZZZabslll15+CWaYYo5JZplmnolmmmquyWabbr4JZ5xyzklnnXbeiWeeeu7JZ59+/glooIIOSmihhh6KaKKKLspoo44+Cmmkkk5KaaWWXopppppuymmnnn4Kaqiijkpqqaaeimqqqq7KaquuvgprrLLOSmuttt6Ka6667sprr77+Cmywwg5LbLHGHov+bLLKLstss84+C2200k5LbbXWXottttpuy2233n4Lbrjijktuueaei2666q7LbrvuvgtvvPLOS2+99t6Lb7767stvv/7+C3DAAg9McMEGH4xwwgovzHDDDj8MccQST0xxxRZfjHHGGm/McccefwxyyCKPTHLJJp+Mcsoqr8xyyy6/DHPMMs9Mc80234xzzjrvzHPPPv8MdNBCD0100UYfjXTSSi/NdNNOPw111FJPTXXVVl+NddZab811115/DXbYYo9Ndtlmn4122mqvzXbbbr8Nd9xyz0133XbfjXfeeu/Nd99+/w144IIPTnjhhh+OeOKKL854444/Dnnkkk/+Tnnlll+Oeeaab855555/Dnrooo9Oeummn4566qqvznrrrr8Oe+yyz0577bbfjnvuuu/Oe+++/w588MIPT3zxxh+PfPLKL898884/D3300k9PffXWX4999tpvz3333n8Pfvjij09++eafj3766q/Pfvvuvw9//PLPT3/99t+Pf/76789///7/D8AACnCABCygAQ+IwAQqcIEMbKADHwjBCEpwghSsoAUviMEManCDHOygBz8IwhCKcIQkLKEJT4jCFKpwhSxsoQtfCMMYynCGNKyhDW+IwxzqcIc87KEPfwjEIApxiEQsohGPiMQkKnGJTGyiE58IxShKcYpUrKKDFa+IxSxqcYtc7KIXvwjGMIpxjGQsoxnPiMY0qnGNbGyjG98IxzjKcY50rKMd74jHPOpxj3zsox//CMhACnKQhCykIQ+JyEQqcpGMbKQjHwnJSEpykpSspCUviclManKTnOykJz8JylCKcpSkLKUpT4nKVKpylaxspStfCctYynI8BQAAOw==" data-src="img/main-slide.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img class="lazy" src="data:image/gif;base64,R0lGODlh+AQ1AoAAAP///wAAACH5BAEAAAEALAAAAAD4BDUCAAL+jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8b+jRw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP778+fTr27+PP7/+/fz++/v/D2CAAg5IYIEGHohgggouyGCDDj4IYYQSTkhhhRZeiGGGGm7IYYcefghiiCKOSGKJJp6IYooqrshiiy6+CGOMMs5IY4023ohjjjruyGOPPv4IZJBCDklkkUYeiWSSSi7JZJNOPglllFJOSWWVVl6JZZZabslll15+CWaYYo5JZplmnolmmmquyWabbr4JZ5xyzklnnXbeiWeeeu7JZ59+/glooIIOSmihhh6KaKKKLspoo44+Cmmkkk5KaaWWXopppppuymmnnn4Kaqiijkpqqaaeimqqqq7KaquuvgprrLLOSmuttt6Ka6667sprr77+Cmywwg5LbLHGHov+bLLKLstss84+C2200k5LbbXWXottttpuy2233n4Lbrjijktuueaei2666q7LbrvuvgtvvPLOS2+99t6Lb7767stvv/7+C3DAAg9McMEGH4xwwgovzHDDDj8MccQST0xxxRZfjHHGGm/McccefwxyyCKPTHLJJp+Mcsoqr8xyyy6/DHPMMs9Mc80234xzzjrvzHPPPv8MdNBCD0100UYfjXTSSi/NdNNOPw111FJPTXXVVl+NddZab811115/DXbYYo9Ndtlmn4122mqvzXbbbr8Nd9xyz0133XbfjXfeeu/Nd99+/w144IIPTnjhhh+OeOKKL854444/Dnnkkk/+Tnnlll+Oeeaab855555/Dnrooo9Oeummn4566qqvznrrrr8Oe+yyz0577bbfjnvuuu/Oe+++/w588MIPT3zxxh+PfPLKL898884/D3300k9PffXWX4999tpvz3333n8Pfvjij09++eafj3766q/Pfvvuvw9//PLPT3/99t+Pf/76789///7/D8AACnCABCygAQ+IwAQqcIEMbKADHwjBCEpwghSsoAUviMEManCDHOygBz8IwhCKcIQkLKEJT4jCFKpwhSxsoQtfCMMYynCGNKyhDW+IwxzqcIc87KEPfwjEIApxiEQsohGPiMQkKnGJTGyiE58IxShKcYpUrKKDFa+IxSxqcYtc7KIXvwjGMIpxjGQsoxnPiMY0qnGNbGyjG98IxzjKcY50rKMd74jHPOpxj3zsox//CMhACnKQhCykIQ+JyEQqcpGMbKQjHwnJSEpykpSspCUviclManKTnOykJz8JylCKcpSkLKUpT4nKVKpylaxspStfCctYynI8BQAAOw==" data-src="img/main-slide.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img class="lazy" src="data:image/gif;base64,R0lGODlh+AQ1AoAAAP///wAAACH5BAEAAAEALAAAAAD4BDUCAAL+jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8b+jRw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP778+fTr27+PP7/+/fz++/v/D2CAAg5IYIEGHohgggouyGCDDj4IYYQSTkhhhRZeiGGGGm7IYYcefghiiCKOSGKJJp6IYooqrshiiy6+CGOMMs5IY4023ohjjjruyGOPPv4IZJBCDklkkUYeiWSSSi7JZJNOPglllFJOSWWVVl6JZZZabslll15+CWaYYo5JZplmnolmmmquyWabbr4JZ5xyzklnnXbeiWeeeu7JZ59+/glooIIOSmihhh6KaKKKLspoo44+Cmmkkk5KaaWWXopppppuymmnnn4Kaqiijkpqqaaeimqqqq7KaquuvgprrLLOSmuttt6Ka6667sprr77+Cmywwg5LbLHGHov+bLLKLstss84+C2200k5LbbXWXottttpuy2233n4Lbrjijktuueaei2666q7LbrvuvgtvvPLOS2+99t6Lb7767stvv/7+C3DAAg9McMEGH4xwwgovzHDDDj8MccQST0xxxRZfjHHGGm/McccefwxyyCKPTHLJJp+Mcsoqr8xyyy6/DHPMMs9Mc80234xzzjrvzHPPPv8MdNBCD0100UYfjXTSSi/NdNNOPw111FJPTXXVVl+NddZab811115/DXbYYo9Ndtlmn4122mqvzXbbbr8Nd9xyz0133XbfjXfeeu/Nd99+/w144IIPTnjhhh+OeOKKL854444/Dnnkkk/+Tnnlll+Oeeaab855555/Dnrooo9Oeummn4566qqvznrrrr8Oe+yyz0577bbfjnvuuu/Oe+++/w588MIPT3zxxh+PfPLKL898884/D3300k9PffXWX4999tpvz3333n8Pfvjij09++eafj3766q/Pfvvuvw9//PLPT3/99t+Pf/76789///7/D8AACnCABCygAQ+IwAQqcIEMbKADHwjBCEpwghSsoAUviMEManCDHOygBz8IwhCKcIQkLKEJT4jCFKpwhSxsoQtfCMMYynCGNKyhDW+IwxzqcIc87KEPfwjEIApxiEQsohGPiMQkKnGJTGyiE58IxShKcYpUrKKDFa+IxSxqcYtc7KIXvwjGMIpxjGQsoxnPiMY0qnGNbGyjG98IxzjKcY50rKMd74jHPOpxj3zsox//CMhACnKQhCykIQ+JyEQqcpGMbKQjHwnJSEpykpSspCUviclManKTnOykJz8JylCKcpSkLKUpT4nKVKpylaxspStfCctYynI8BQAAOw==" data-src="img/main-slide.jpg" alt="">
                            </div>
                        </div>
                        <div class="custom-fraction-wrap">
                            <div class="custom-current"></div>
                            <div class="color-white">&nbsp;&nbsp;/&nbsp;&nbsp;</div>
                            <div class="custom-total"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="collection-popular">
            <div class="spacer-120"></div>
            <div class="container">
                <div class="block-header">
                    <h2 class="h2 title">жіноча колекція весна 2021</h2>
                    <a href="" class="link">Дивитись більше</a>
                </div>
                <div class="collection-list">
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-1.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">взуття</a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-2.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">сумки</a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-3.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">Кросівки</a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-120"></div>
        </section>

        <section class="collection-popular bg-1 type-2">
            <div class="spacer-120"></div>
            <div class="container swiper-entry">
                <div class="block-header">
                    <h2 class="h2 title">жіноча колекція взуття</h2>
                    <a href="" class="link">Дивитись більше</a>
                </div>
                <div class="collection-products type-2">
                    <div class="swiper-button-prev button"><img src="img/slide-arrow-left.svg" alt=""></div>
                    <div class="swiper-button-next button"><img src="img/slide-arrow-right.svg" alt=""></div>
                    <div class="swiper-container" data-options='{"slidesPerView":4,"spaceBetween":35, "breakpoints":{"991": {"slidesPerView":3,"spaceBetween":12}, "767": {"slidesPerView":2,"spaceBetween":12}}}'>

                        <div class="swiper-wrapper">
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-4.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-5.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-6.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-7.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-6.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                            <div class="item swiper-slide">
                                <a href="" class="item-link"></a>
                                <div class="image">
                                    <img class="lazy" data-src="img/products/product-7.jpg" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                    <div class="price">₴ 3249</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-scrollbar"></div>
                    </div>
                </div>
            </div>
            <div class="spacer-120"></div>
        </section>

        <section class="collection-popular">
            <div class="spacer-120"></div>
            <div class="container">
                <div class="block-header">
                    <h2 class="h2 title">Чоловіча колекція весна</h2>
                    <a href="" class="link">Дивитись більше</a>
                </div>
                <div class="collection-list">
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-8.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">Кросівки </a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-9.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">взуття</a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                    <div class="item">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product-10.jpg" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="item-title">туфлі</a>
                            <p>OUTLET 2020 / 21</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-120"></div>
        </section>

        <section class="season-info bg-1">
            <div class="spacer-120"></div>
            <div class="container">
                <a href="" class="image">
                    <img class="lazy" data-src="img/image-1.png" alt="">
                </a>
                <div class="text">
                    <div class="season-year">spring 20/21</div>
                    <div class="middle-text">
                        <div class="season-title">POP OF <br>color</div>
                        <p>Час обирати <br>яскраве взуття на весну.</p>
                    </div>
                    <a href="" class="link-detail">
                        Перейти до колекції
                        <span class="icon">
                            <img src="img/arrow-long-right.svg" alt="">
                        </span>
                    </a>
                </div>
            </div>
            <div class="spacer-120"></div>
        </section>

        <section class="index-products">
            <div class="spacer-75"></div>
            <div class="container">
                <div class="tabs">
                    <div class="tabs-nav">
                        <a href="" class="active" data-tab="tab-1">Акції</a>
                        <a href="" data-tab="tab-2">Знижки</a>
                        <a href="" data-tab="tab-3">Топ продаж</a>
                        <a href="" data-tab="tab-4">Тренди</a>
                    </div>
                    <div class="tab-item tab-1 active swiper-entry">
                        <div class="collection-products">
                            <div class="swiper-button-prev button"><img src="img/slide-arrow-left.svg" alt=""></div>
                            <div class="swiper-button-next button"><img src="img/slide-arrow-right.svg" alt=""></div>
                            <div class="swiper-container" data-options='{"slidesPerView":5,"spaceBetween":8, "breakpoints":{"991": {"slidesPerView":4,"spaceBetween":12}, "767": {"slidesPerView":2,"spaceBetween":12}}}'>

                                <div class="swiper-wrapper">
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-11.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-12.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-13.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-11.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-12.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-13.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-item tab-2 swiper-entry">
                        <div class="collection-products">
                            <div class="swiper-button-prev button"><img src="img/slide-arrow-left.svg" alt=""></div>
                            <div class="swiper-button-next button"><img src="img/slide-arrow-right.svg" alt=""></div>
                            <div class="swiper-container" data-options='{"slidesPerView":5,"spaceBetween":8, "breakpoints":{"991": {"slidesPerView":4,"spaceBetween":12}, "767": {"slidesPerView":2,"spaceBetween":12}}}'>

                                <div class="swiper-wrapper">
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-11.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-12.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-13.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-11.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-12.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                    <div class="item swiper-slide">
                                        <a href="" class="item-link"></a>
                                        <div class="image">
                                            <img class="lazy" data-src="img/products/product-13.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <a href="" class="item-title">LE'BERDES черевики осінні</a>
                                            <div class="price">₴ 3249</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-120"></div>
        </section>

        <div class="prod-seo bg-1">
            <div class="spacer-120"></div>
            <div class="container">
                <div class="prodList-seo">
                    <div class="item item-1">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product_14.jpg" alt="">
                        </div>
                        <div class="info text-center">
                            <div class="title">POP OF COLOR</div>
                            <p>Час обирати яскраве взуття на весну.</p>
                        </div>
                    </div>
                    <div class="item item-2">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product_14.jpg" alt="">
                        </div>
                        <div class="info text-right">
                            <div class="title">ЧЕЛСІ НА КОЖЕН ДЕНЬ</div>
                            <p>Перфоровані, високі чи лаковані - які оберете для себе?</p>
                        </div>
                    </div>
                    <div class="item item-3">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product_14.jpg" alt="">
                        </div>
                        <div class="info text-left">
                            <div class="title">ЧЕЛСІ НА КОЖЕН ДЕНЬ</div>
                            <p>Широкі підбори в 2021 залишаються з нами.</p>
                        </div>
                    </div>
                    <div class="item item-4">
                        <a href="" class="item-link"></a>
                        <div class="image">
                            <img class="lazy" data-src="img/products/product_14.jpg" alt="">
                        </div>
                        <div class="info text-center">
                            <div class="title">MADE IN ITALY</div>
                            <p>Нова колекція аксесуарів з Флоренції.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-80"></div>
        </div>

        <div class="brands swiper-entry bg-1">
            <div class="container">
                <div class="swiper-container" data-options='{"slidesPerView":4,"spaceBetween":35, "autoplay":true, "loop": true, "breakpoints":{"991": {"slidesPerView":3,"spaceBetween":12}, "767": {"slidesPerView":2,"spaceBetween":12}}}'>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" src="img/brand/b-1.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" src="img/brand/b-2.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" src="img/brand/b-3.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" src="img/brand/b-1.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" src="img/brand/b-2.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" data-src="img/brand/b-3.png" alt=""></a>
                        </div>
                        <div class="swiper-slide">
                            <a href=""><img class="lazy" data-src="img/brand/b-2.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="index-reviews bg-1">
            <div class="spacer-75"></div>
            <div class="container  swiper-entry">
                <div class="block-header text-center">
                    <div class="block-title">відгуки та запитання</div>
                    <a href="" class="link">Перейти до всіх відгуків</a>
                </div>

                <div class="review-slider swiper-container" data-options='{"slidesPerView":3,"spaceBetween":35, "breakpoints":{"991": {"slidesPerView":2,"spaceBetween":16}, "767": {"slidesPerView":1,"spaceBetween":16}}}'>

                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-1.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-2.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-1.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-2.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-1.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="review">
                                <div class="review-header">
                                    <div class="image">
                                        <img class="lazy" data-src="img/reviews/prod-1.png" alt="">
                                    </div>
                                    <div class="prod-info">
                                        <a href="" class="review-title">SASHA FABIANI ТУФЛІ</a>
                                        <div class="review-date">24 Лютого 2020</div>
                                    </div>
                                </div>
                                <div class="text">
                                    Очень качественные туфли, обожаю их и рекомендую. Очень удобные, на каждый день - бегаю в них как. Очень качественные туфли, обожаю их и рекомендую.
                                </div>
                                <div class="review-bottom">
                                    <div class="name">Влада</div>
                                    <div class="rate">
                                        <img src="img/star.svg" alt="">
                                        <span>5.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>


            </div>
            <div class="spacer-75"></div>
        </div>

        <div class="seo-block bg-1">
            <div class="simple-page container">
                <h1 style="text-align: center;">Купити взуття вигідно та по-сучасному в інтернет-магазині Favorite Shoes у Києві, Львові та інших містах України</h1>

                <p>&nbsp;</p>

                <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень. З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>

                <p>Більше не потрібно виходити з дому. Варто тільки обрати модель і розмір та зробити замовлення. Кілька днів і ваші ніжки в обновці.</p>

                <h2>Різновиди моделей в інтернет-магазині взуття в Україні</h2>

                <p>І жінка, і чоловік завжди прагнуть виглядати неперевершено, тому кількість пар черевиків в гардеробі буває незліченною. На роботу, в магазин, на прогулянку, в гості &ndash; це все різні взуттєві вироби. А ще зимові, літні та демісезонні&hellip;</p>

                <p>Охопити всі різновиди дуже важко, тому розглянемо найпопулярніші.</p>

                <ul>
                    <li>
                        <p>Чоботи. Взимку це найкращий варіант бути красивою і не замерзнути. Якісні моделі в магазинах взуття в Києві, Львові, Дніпрі мають натуральну підкладку, яка зберігає тепло. Чоловічі різновиди можуть бути на плоскій підошві або з 2 см каблуком. Такого плану є моделі і для жінок, а також з підборами різної висоти, товщини та форми. Довжина халяви теж відрізняється, як і її ширина. Обирайте те, що вам до смаку та точно буде комфортним для пересування по зимових тротуарах.</p>
                    </li>
                    <li>
                        <p>Черевики. Звісно ж магазини взуття в Києві та Львові пропонують і весняні моделі, і тепліші.&nbsp; Актуальною така пара буде в весняно-осінній період поки немає снігу. Через низьку халяву може засипатися сніг та проникати холодне повітря, що не дуже зручно. Тип підошви буває різним, тому обрати є з чого. Високі підбори, тракторний протектор, повна відсутність каблука або танкетка &ndash; будь-що для вашого задоволення. Найчастіше такі моделі на низькому ходу обирають чоловіки, адже вони практичні, бо підходять під повсякденний і офісний стиль.</p>
                    </li>
                </ul>

                <p>Каталог інтернет-магазину Favorite Shoes здатен приємно здивувати цінами на взуття від брендових виробників. У нас ви знайдете шикарні вироби від Alpino, Anemone, Aici Berlucci, Bazallini, Le&#39;berdes та багато ін. Зробіть собі подарунок.</p>

                <ol>
                    <li value="3">
                        <p>Мокасини &ndash; один із універсальних варіантів черевиків, адже існують як літні, так і зимові моделі. Останні дещо не зовсім практичні і більше підходять для комфортного керування автомобілем. Обійдуться вони недорого, а зручність гарантована і жінкам, і чоловікам.</p>
                    </li>
                    <li value="4">
                        <p>Туфлі &ndash; невід&rsquo;ємна частина офісного та святкового стилю. Черевики-лодочки на підборах давно вже стали класикою і вдало поєднуються як із штанами, так і зі спідницями різного фасону. Це пояснює велику кількість охочих купити взуття в Києві та Львові. Воно стане ідеальним доповненням вечірньої сукні, легкого весняного пальта, білої блузи та приталених однотонних штанів.</p>
                    </li>
                </ol>

                <p>Серед чоловіків найпопулярнішими є класичні туфлі зі шкіри, часто з лакованої. Вони одягаються на всі випадки життя і поєднуються з різноплановим вбранням.</p>

                <ol>
                    <li value="5">
                        <p>Сандалі &ndash; приклад практичності та й ціни на взуття такого плану і для чоловіків, і для жінок найнижчі. Це найліпша пара для прогулянок та відпочинку на природі, для походів по магазинах або кафе. Ноги точно не втомляться, не спітніють та виглядатимуть привабливо.</p>
                    </li>
                </ol>

                <h2>Де&nbsp;купити взуття у Львові, Харкові, Одесі та інших містах країни?</h2>

                <p>Сайт Favorite Shoes не перестає дивувати своїх клієнтів, тому пропонує взяти участь у дисконтній програмі. Чим більше ви купуєте, тим вищу знижку отримуєте. З нами купити взуття відмінної якості набагато простіше та безпечно.</p>

                <p>Оскільки підібрати на ногу без примірки модель досить тяжко, відправляємо замовлення післяплатою через Нову Пошту. Отримали, приміряли і оплатили лише те, що підійшло. Наша діяльність охоплює всю територію України, тому продаж взуття, та&nbsp; доставка в Київ або Чернігів &ndash; не проблема. Також є самовивіз зі Львова.</p>

                <p>Пам&rsquo;ятайте, що дешеве взуття довго не протримається, тому надавайте перевагу перевіреним брендам та магазинам.</p>


                <div class="seo-hidden">
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                </div>
                <div class="seo-arrow"><img src="img/seo-arrow.svg" alt=""></div>
            </div>
        </div>

        <div class="actions-index bg-1">
            <div class="spacer-75"></div>
            <div class="container swiper-entry">
                <div class="block-header text-center">
                    <div class="block-title">Новини та акції</div>
                    <a href="" class="link">всі записи</a>
                </div>
                <div class="actions-slider swiper-container" data-options='{"slidesPerView":1, "loop":true, "spaceBetween":100, "breakpoints":{"991": { "spaceBetween":30,"slidesPerView":2}, "580": {"slidesPerView":1,"loop":false,"spaceBetween":11}}}'>
                    <div class="swiper-button-prev button"><img src="img/arrow-long-right.svg" alt=""></div>
                    <div class="swiper-button-next button"><img src="img/arrow-long-right.svg" alt=""></div>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="action">
                                <a href="" class="image">
                                    <img class="lazy" src="img/action-img.jpg" alt="">
                                    <span class="date">20/01/2021</span>
                                </a>

                                <a href="" class="title">Які кросівки купити на літо 2021?</a>
                                <p>Розбираємося у всіх тонкощах вибору</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="action">
                                <a href="" class="image">
                                    <img class="lazy" src="img/action-img.jpg" alt="">
                                    <span class="date">20/01/2021</span>
                                </a>

                                <a href="" class="title">Які кросівки купити на літо 2021?</a>
                                <p>Розбираємося у всіх тонкощах вибору</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="action">
                                <a href="" class="image">
                                    <img class="lazy" src="img/action-img.jpg" alt="">
                                    <span class="date">20/01/2021</span>
                                </a>

                                <a href="" class="title">Які кросівки купити на літо 2021?</a>
                                <p>Розбираємося у всіх тонкощах вибору</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="action">
                                <a href="" class="image">
                                    <img class="lazy" src="img/action-img.jpg" alt="">
                                    <span class="date">20/01/2021</span>
                                </a>

                                <a href="" class="title">Які кросівки купити на літо 2021?</a>
                                <p>Розбираємося у всіх тонкощах вибору</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
            </div>
            <div class="spacer-75"></div>
        </div>



    </div>



    <!-- INSTAGRAM -->
    <section>
        <? include '_instagram-block.php';?>
    </section>

    <!-- SUBSCRIBE -->
    <section>
        <? include '_subscribe.php';?>
    </section>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <? include '_bottom.php';?>

</body>

</html>
