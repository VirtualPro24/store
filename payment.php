<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Оплата та доставка</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page pd-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Оплата та доставка</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>



                <div class="payment-row">
                    <div class="h1 page-title fw-500">Оплата та доставка</div>
                    <p class="description">Оплатити замовлення можна одим зі зручних
                        для Вас способів:</p>

                    <div class="payment-list">
                        <div class="item">
                            <div class="icon">
                                <img src="img/pay/privat.png" alt="">
                            </div>
                            <p>Оплата через <b>Приват24</b> (реквізити будуть відправлені Вам по смс, в Viber або e-mail)</p>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="img/pay/Visa.svg" alt="">
                                <img src="img/pay/Mastercard.svg" alt="">
                            </div>
                            <p>Оплата карткою <b>Visa/MasterCard</b>
                                на сайті</p>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="img/pay/Nova_Poshta.svg" alt="">
                            </div>
                            <p>Накладним плетежем при отриманні ( <b>+2%</b> від суми замовлення <b>+20грн</b> - згідно тарифів Нової Пошти)
                            </p>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="img/pay/cash.svg" alt="">
                            </div>
                            <div class="h2">Оплата готівкою </div>
                            <p>в магазині <br>
                                (у випадку cамовивозу)</p>
                        </div>
                    </div>
                </div>

                <div class="delivery-row">
                    <div class="delivery-header h1 fw-500">
                        <div class="icon"><img src="img/delivery-icon.svg" alt=""></div>
                        Доставка
                    </div>

                    <div class="delivery-list">
                        <p>Відправляємо Ваші замовлення по Україні компанією «Нова Пошта» на відділення або кур’єром на адресу.</p>
                        <p><b>БЕЗКОШТОВНО*</b> доставляємо замовлення, які були оплачені online на сайті або на картку (реквізити надсилаємо після підтвердження замовлення).
                        </p>
                        <p>*мінімальна сума замовлення – <b>500 грн</b> (якщо сума замовлення до <b>500 грн</b> – доставку оплачує отримувач згідно з тарифами компанії).</p>
                        <p>Також, Ви можете обрати доставку НАКЛАДНИМ ПЛАТЕЖЕМ та оплатити замовлення на пошті після огляду та примірки. Вартість доставки оплачує отримувач згідно з тарифами перевізника. Враховуйте додаткову комісію компанії «Нова Пошта» за переказ коштів (20 грн + 2% від суми замовлення).</p>
                        <p>САМОВИВІЗ з наших магазинів у Львові – безкоштовно.</p>
                        <p>Друзі, ми чудово розуміємо, що обрати взуття без примірки – складно. Ви можете замовити 2 пари взуття на вибір. Приміряєте, оглядаєте замовлення та оплачуєте лише те, що Вам підійшло.</p>
                        <p>Адміністрація залишає за собою право переглядати умови доставки в індивідуальному порядку.</p>
                        <p>Якщо у Вас є запитання по доставці – телефонуйте <b>0 800 216 959 </b>(понеділок - субота з
                            <b>10 до 19</b>).</p>
                    </div>
                </div>
                
                <div class="delivery-row">
                    <div class="delivery-header h1 fw-500">
                        <div class="icon"><img src="img/pay/icon-wallet.svg" alt=""></div>
                        Оплата замовлень за кордон
                    </div>
                    
                    <div class="text">
                        <div class="h2">Оплатити замовлення можна карткою Visa/MasterCard* на сайті (у зручній для Вас валюті, конвертація відбувається відповідно до курсу валют Вашого банку)</div>
                        <div class="warning">*сума переказу коштів розраховується індивідуально менеджером, врахувавши вартість за доставку замовлення до Вас</div>
                    </div>

                    <div class="delivery-list"> 
                        <p>При отриманні товару необхідно мати при собі документ, що засвідчує особу (паспорт або водійське посвідчення).</p>
                        <p>Замовлення, підтверджені до 15:00, відправляються в цей самий день. У разі підтвердження після 15:00 замовлення відправляються наступного дня.</p>
                        <p> У випадку доставки кур'єрською службою "Нова Пошта" Ви отримуєте номер транспортної накладної (по смс, в Viber або e-mail) та можете відстежувати статус доставки замовлення.</p>
                    </div>
                </div>                

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
