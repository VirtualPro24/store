<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Сторінку не знайдено</title>
</head>

<body class="home-page">

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header bg-1"></div>

        <div class="not-found bg-1">
            <div class="content">
                <div class="h2 title">Упс помилка, ми нічого не знайшли. Будь-ласка, спробуйте ще або <a href="">введіть інший</a> запит</div>
                <img src="img/404.png" alt="">
                <div class="btn-group">
                    <a href="" class="btn btn-stroke">Вернутись в магазин</a>
                    <a href="" class="link">Мапа сайту</a>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
