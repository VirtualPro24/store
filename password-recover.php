<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Увiйти</title>
</head>

<body class="home-page">

    <div id="content-block" class="bg-1">

        <div class="sign page">
            <div class="sign-header">
                <a href="categories.php" class="link-back">Назад в магазин</a>
                <a href="" class="logo only-mob">
                    <img src="img/logo.svg" alt="">
                </a>
            </div>
            <div class="sign-page-content">
                <div class="sign-row">
                    <div class="sign-image sign-col">
                        <img src="img/sign/sign-img.jpg" alt="">
                    </div>
                    <div class="sign-col sign-content recover bg-2">
                        <a href="" class="logo hidden-xs">
                            <img src="img/logo.svg" alt="">
                        </a>
                        <div class="title h2 type-2">Вiдновлення паролю</div>
                        <div class="sign-form">
                            <form action="">
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/icon-email.svg" alt="" class="icon">
                                    <label for="t1" class="input-title">E-mail</label>
                                    <input type="email" id="t1" class="input">
                                </div>
                                <button class="btn btn-primary disabled" disabled>Вiдновити пароль</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
