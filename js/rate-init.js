jQuery(function ($) {

    "use strict";

    $('.rater').each(function () {
        var $this = $(this).get(0);
        var container = $(this).closest('.rate-wrap');
        var starRatingStep = raterJs({
            starSize: 16,
            step: 0.5,
            element: $this,
            rateCallback: function rateCallback(rating, done) {
                this.setRating(rating);
                done();
                container.find('.rater-input').val(rating);
                container.find('.rater-count').text(rating);
            }
        });
    });

});
