var _functions = {},
    winWidth;

jQuery(function ($) {

    "use strict";

    /* function on page ready */
    var isTouchScreen = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);
    if (isTouchScreen) $('html').addClass('touch-screen');
    var winScr, winHeight,
        is_Mac = navigator.platform.toUpperCase().indexOf('MAC') >= 0,
        is_IE = /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/\d+/.test(navigator.userAgent),
        is_Chrome = navigator.userAgent.indexOf('Chrome') >= 0 && navigator.userAgent.indexOf('Edge') < 0;

    /* check browser */
    winWidth = $(window).width();
    winHeight = $(window).height();

    if (is_Mac) {
        $('html').addClass('mac');
    }
    if (is_IE) {
        $('html').addClass('ie');
    }
    if (is_Chrome) {
        $('html').addClass('chrome');
    }

    /* page loader and etc */
    if ($('.SelectBox:not(.search)').length) {
        $('.SelectBox:not(.search)').SumoSelect();

    }
    if ($('.SelectBox.search').length) {
        var text = $('.SelectBox.search').attr('data-search');
        $('.SelectBox.search').SumoSelect({
            search: true,
            searchText: text
        });
    }


    /* function on page scroll */
    var lastScrollTop = 0;
    $(window).scroll(function () {
        _functions.scrollCall();
        var st = $(this).scrollTop();
        if (st > lastScrollTop && winScr > 190) {
            $('header').addClass('hide');
        } else {
            $('header').removeClass('hide');
        }

        if ($('.card-mobile-fixed').length) {
            if (st > lastScrollTop) {
                $('.card-mobile-fixed').addClass('active');
            } else {
                $('.card-mobile-fixed').removeClass('active');
            }
        }
        lastScrollTop = st;
    });

    _functions.scrollCall = function () {
        winScr = $(window).scrollTop();
        if (winScr > 10) {
            $('header').addClass('scrolled');
        } else {
            $('header').removeClass('scrolled');
        }

        var navHeight = $('.top-section').height();
        if ($(window).scrollTop() > navHeight) {
            $('.anchors-nav').addClass('fixed');
        } else {
            $('.anchors-nav').removeClass('fixed');
        }
    }

    //layer close
    $('.layer-close').on('click', function () {
        $('header').removeClass('active-layer-close');
        $('.user-account').removeClass('open');
        $('.menu-notification').removeClass('open');
    });



    //custom-input-number
    $('.custom-input-number .increment').on('click', function () {
        var $input = $(this).siblings('.input'),
            val = parseInt($input.val()),
            max = parseInt($input.attr('max')),
            step = parseInt($input.attr('step'));
        var temp = val + step;
        $input.val(temp <= max ? temp : max);
    });

    $('.custom-input-number .decrement').on('click', function () {
        var $input = $(this).siblings('.input'),
            val = parseInt($input.val()),
            min = parseInt($input.attr('min')),
            step = parseInt($input.attr('step'));
        var temp = val - step;
        $input.val(temp >= min ? temp : min);
    });

    //focus input
    $('.input-field-wrapp .input').on('focus', function () {
        $(this).closest('.input-field-wrapp').addClass('focus');
    });
    $('.input-field-wrapp .input').on('blur', function () {
        $(this).closest('.input-field-wrapp').removeClass('focus');
    });
    $('.input-field-wrapp .input').on('keyup', function () {
        if ($(this).val()) $(this).parent().addClass('value');
        else $(this).parent().removeClass('value');
    });
    $('.input-field-wrapp select').on('change', function () {
        $(this).parent().addClass('value');
    });

    //fail Input
    $('.input').on('keyup', function () {
        if ($(this).val()) {
            $(this).parent('.input-field-wrapp').removeClass('fail');
        } else {
            $(this).parent('.input-field-wrapp').addClass('fail');
        }
    });

    /* swiper sliders */
    _functions.getSwOptions = function (swiper) {
        var options = swiper.data('options');
        options = (!options || typeof options !== 'object') ? {} : options;
        var $p = swiper.closest('.swiper-entry'),
            slidesLength = swiper.find('>.swiper-wrapper>.swiper-slide').length;
        if (!options.pagination) options.pagination = {
            el: $p.find('.swiper-pagination')[0],
            clickable: true
        };

        //    if (!options.autoplay) options.autoplay = {
        //          delay: 2500,
        //          disableOnInteraction: false,
        //    }; 
        if (!options.scrollbar) options.scrollbar = {
            el: $p.find('.swiper-scrollbar')[0],
            hide: false,
        };

        if (!options.navigation) options.navigation = {
            nextEl: $p.find('.swiper-button-next')[0],
            prevEl: $p.find('.swiper-button-prev')[0]
        };
        options.preloadImages = false;
        options.lazy = {
            loadPrevNext: true
        };
        options.observer = true;
        options.observeParents = true;
        options.watchOverflow = true;
        if (!options.speed) options.speed = 500;
        options.roundLengths = false;
        if (!options.centerInsufficientSlides) options.centerInsufficientSlides = false;
        if (options.customFraction) {
            $p.addClass('custom-fraction-swiper');
            if (slidesLength > 1 && slidesLength < 10) {
                $p.find('.custom-current').text('01');
                $p.find('.custom-total').text('0' + slidesLength);
            } else if (slidesLength > 1) {
                $p.find('.custom-current').text('01');
                $p.find('.custom-total').text(slidesLength);
            }
        }
        if (isTouchScreen) options.direction = "horizontal";

        if (options.initialLast) options.initialSlide = slidesLength;

        if (options.customPagination) {
            options.pagination.renderBullet = function (index, className) {
                var slide = swiper.find('.swiper-slide').eq(index);
                if (slide.data('video')) className += ' video';
                return '<span class="' + className + ' custom" style="background-image: url(' + slide.data('preview') + ')"><span></span></span>';
            }
        }

        if (options.progressbar) {
            options.pagination.type = 'progressbar';
        }
        if (options.customFraction) {
            $p.addClass('custom-fraction-swiper');
            if (slidesLength > 1 && slidesLength < 10) {
                $p.find('.custom-current').text('01');
                $p.find('.custom-total').text('0' + slidesLength);
            } else if (slidesLength > 1) {
                $p.find('.custom-current').text('01');
                $p.find('.custom-total').text(slidesLength);
            }
        }
        return options;
    };

    _functions.initSwiper = function (el) {
        var swiper = new Swiper(el[0], _functions.getSwOptions(el));
    };

    $('.swiper-entry .swiper-container').each(function () {
        _functions.initSwiper($(this));
    });

    $('.swiper-thumbs').each(function () {
        var top = $(this).find('.swiper-container.swiper-thumbs-top')[0].swiper,
            bottom = $(this).find('.swiper-container.swiper-thumbs-bottom')[0].swiper;
        top.thumbs.swiper = bottom;
        top.thumbs.init();
        top.thumbs.update();
    });

    $('.swiper-control').each(function () {
        var top = $(this).find('.swiper-container')[0].swiper,
            bottom = $(this).find('.swiper-container')[1].swiper;
        top.controller.control = bottom;
        bottom.controller.control = top;
    });
    //custom fraction
    $('.custom-fraction-swiper').each(function () {
        var $this = $(this),
            $thisSwiper = $this.find('.swiper-container')[0].swiper;

        $thisSwiper.on('slideChange', function () {
            $this.find('.custom-current').text(
                function () {
                    if ($thisSwiper.realIndex < 9) {
                        return '0' + ($thisSwiper.realIndex + 1)
                    } else {
                        return $thisSwiper.realIndex + 1
                    }
                }
            )
        });
    });

    //popup
    var popupTop = 0;
    _functions.removeScroll = function () {
        popupTop = $(window).scrollTop();
        $('html').css({
            "position": "fixed",
            "top": -$(window).scrollTop(),
            "width": "100%"
        });
    }
    _functions.addScroll = function () {
        $('html').css({
            "position": "static"
        });
        window.scroll(0, popupTop);
    }
    _functions.openPopup = function (popup) {
        $('.popup-content').removeClass('active');
        $(popup + ', .popup-wrapper').addClass('active');
        _functions.removeScroll();
    };

    _functions.closePopup = function () {
        $('.popup-wrapper, .popup-content').removeClass('active');
        setTimeout(function () {
            $('.popup-content[data-rel="video-popup"] .popup-video-wrapper').html('');
        }, 300);
        _functions.addScroll();
    };



    $(document).on('click', '.open-popup', function (e) {
        e.preventDefault();
        _functions.openPopup('.popup-content[data-rel="' + $(this).data('rel') + '"]');
    });

    $(document).on('click', '.popup-wrapper .btn-close, .popup-wrapper .layer-close, .popup-wrapper .close-popup', function (e) {
        e.preventDefault();
        _functions.closePopup();
    });

    //close popup with ESCAPE key
    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            _functions.closePopup();
        }
    });

    // Open video popup
    $(document).on('click', '.open-video', function () {
        var video = '<iframe class="video" src="' + $(this).data('video') + '" style="width: 100%; height: 100%;" frameborder="0" allow="autoplay" allowfullscreen></iframe>';
        //var video = '<iframe src="https://players.brightcove.net/'+ $(this).data("brightcore-account") + '/'+ $(this).data('player') +'_default/index.html?videoId=' + $(this).data("video-id") + '&autoplay=true" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" width="100%" height="100%"></iframe>';
        $('.popup-content[data-rel="' + $(this).data('rel') + '"] .popup-video-wrapper').html(video);
        _functions.openPopup('.popup-content[data-rel="' + $(this).data('rel') + '"]');
        return false;
    });


    /* tabs */
    $('[data-tab]').click(function (e) {
        e.preventDefault();

        var current = $(this).attr('data-tab');

        $(this).closest('.tabs').find('[data-tab]').removeClass('active');
        $(this).addClass('active');

        $(this).closest('.tabs').find('.tab-item').slideUp(0);
        $(this).closest('.tabs').find('.' + current).stop().slideDown(0);

    });

    $('.seo-arrow').click(function (e) {
        e.preventDefault();

        $(this).toggleClass('active');
        $('.seo-hidden').slideToggle();
    });

    $('.form-search .icon').click(function () {
        $('.form-search').toggleClass('active');
        $('.search-wrap').addClass('active');
    });

    $('.phones > a, .phones .arrow').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.phones').toggleClass('active');
        $(this).closest('.phones').find('.contact-info').slideToggle();
    });

    $('.phone-toggle').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).closest('.phone-list').find('ul').slideToggle();
    });
    $(document).on('click', function (e) {
        if (!e.target.closest('.phones')) {
            $('.phones').find('.contact-info').slideUp();
            $('.phones').removeClass('active');
        }
    });


    $('.menu-nav a').on('click', function (e) {
        e.preventDefault();
        var item = $(this).attr('data-nav');

        $('.menu-nav li').removeClass('active');
        $(this).closest('li').addClass('active');

        $('.nav-menu').removeClass('active');
        $('.nav-menu.' + item).addClass('active');
    })

    $('[data-mob-nav]').on('click', function (e) {
        e.preventDefault();
        var item = $(this).attr('data-mob-nav');

        $('[data-mob-nav]').removeClass('active');
        $(this).addClass('active');

        $('.item-nav').removeClass('active');
        $('.item-nav.' + item).addClass('active');
    });

    $('[data-subnav]').on('click', function (e) {
        e.preventDefault();
        var item = $(this).attr('data-subnav');

        $('[data-subnav]').removeClass('active');
        $(this).addClass('active');

        $('.mobile-menu').addClass('active');

        $('.item-subnav').removeClass('active');
        $('.item-subnav.' + item).addClass('active');
    });

    $('.item-subnav .submenu-title').on('click', function (e) {
        e.preventDefault();

        $(this).parent('li').find('ol').slideToggle('active');
        $(this).toggleClass('active');

    });


    $('.mobile-button').click(function (e) {
        e.preventDefault();
        $('html').toggleClass('overflow-hidden');
        $(this).toggleClass('active');
        $('.mobile-menu').slideToggle();
    });

    $('.item-subnav .back').click(function (e) {
        e.preventDefault();
        $(this).closest('.item-subnav').removeClass('active');
        $('.mobile-menu').removeClass('active');
    });

    $('.mobile-filter').click(function (e) {
        e.preventDefault();
        $('.filter--mob').fadeIn();
        $('.filter--mob').addClass('open');
        $('html').toggleClass('overflow-hidden');
    });

    $('.close--filter').click(function (e) {
        e.preventDefault();
        $('.filter--mob').fadeOut();
        $('.filter--mob').removeClass('open');
        $('html').toggleClass('overflow-hidden');
    });

    $('.filter-more').click(function (e) {
        e.preventDefault();

        var text = $(this).attr('data-text');
        var textActive = $(this).attr('data-text-active');

        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).text(textActive);
            $('.filters').find('.select-hide').slideDown();
        } else {
            $(this).removeClass('active');
            $(this).text(text);
            $('.filters').find('.select-hide').slideUp();
        }
    });


    $('.product-images [data-src]').on('mousemove', function () {
        var src = $(this).attr('data-src');

        $(this).closest('.product-images').find('a').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.product-main').find('.image img').attr('src', src);
    });
    $('.product-images').on('mouseleave', function () {
        var src = $(this).find('a:first-child').attr('data-src');

        $(this).find('a').removeClass('active');
        $(this).find('a:first-child').addClass('active');
        $(this).closest('.product-main').find('.image img').attr('src', src);
    });

    $('.add-favorite').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });

    //focus input
    $('.input-wrap .input').on('focus', function () {
        $(this).closest('.input-wrap').addClass('focus');
    });
    $('.input-wrap .input').on('blur', function () {
        $(this).closest('.input-wrap').removeClass('focus');
    });
    $('.input-wrap .input').on('keyup', function () {
        if ($(this).val()) $(this).parent().addClass('value');
        else $(this).parent().removeClass('value');
    });
    $('.input-wrap select').on('change', function () {
        $(this).parent().addClass('value');
    });

    //fail Input
    $('.input').on('keyup', function () {
        if ($(this).val()) {
            $(this).parent('.input-wrap').removeClass('fail');
        } else {
            $(this).parent('.input-wrap').addClass('fail');
        }
    });

    $('.add-to-cart').on('click', function (e) {
        e.preventDefault();
        $('.product-alert').addClass('active');
    });
    $('.alert-close').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.alert').removeClass('active');
        $(this).closest('.main-alert').slideUp();
        var hAlert = $('.main-alert').outerHeight();
        var height = $('.site-header').outerHeight();
        $('.margin-header').css('height', height - hAlert);
    });

    //show review
    $('.show-all-review').on('click', function (e) {
        e.preventDefault();
        $(this).slideUp();
        $('.review-tab-list .review-line.hide').slideDown();
    });

    //telephone mask
    $('input[type="tel"]').on('focus', function () {
        $(this).inputmask({
            "mask": "+389 (99) 999 99 99",
            showMaskOnHover: false,
        });
    });

    var galleryThumbs = new Swiper('.card-thumbs .swiper-container', {
        direction: 'vertical',
        slidesPerView: 4,
        slideToClickedSlide: true,
        loopedSlides: 50,
        loop: true,
        spaceBetween: 20,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: ".card-preview .swiper-button-next",
            prevEl: ".card-preview .swiper-button-prev"
        }
    });
    var galleryMain = new Swiper('.card-gallery', {
        slidesPerView: 1,
        loop: true,
        loopedSlides: 50,
        thumbs: {
            swiper: galleryThumbs
        }
    });


    /*PLUS - MINUS*/
    //    $(".number-count").append('<div class="inc button"></div> ');
    //    $(".number-count").prepend('<div class="dec button"></div>');

    $(".number-count .button").on("click", function () {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }

        $button.parent().find("input").val(newVal);

    });


    $('.mc-tr-remove, .table-row-delete').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('[data-tr-product]'),
            popover = container.find('[data-popover]');
        container.addClass('open');
    });

    $('[data-tr-remove]').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('[data-tr-product]');
        container.remove();
    });
    $('[data-popover-close]').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('[data-tr-product]');
        container.removeClass('open');
    });

    $('.add-comment .btn').on('click', function (e) {
        e.preventDefault();

        $(this).css('display', 'none');
        $(this).closest('.add-comment').find('.add-comment-block').slideDown();
    });

    $('.add-comment-block .input').on('input keyup', function () {
        var lenght = $(this).val().length;

        $(this).closest('.add-comment-block').find('.count-area').text(lenght);
    });


    //    promo code
    $('[data-open-promo]').on('click', function (e) {
        e.preventDefault();
        $(this).fadeOut(0);
        $('[data-promo-item]').slideDown(0);
        $(this).closest('.promo-block').addClass('active');
    })
    $('[data-promo-close]').on('click', function (e) {
        e.preventDefault();
        $('[data-open-promo]').fadeIn(0);
        $('[data-promo-item]').fadeOut(0);
        $(this).closest('.promo-block').removeClass('active');
    })
    $('.apply-wrap').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('.promo-block');
        container.addClass('apply');
        $('.discount-block').addClass('active');
        container.find('input').attr('readonly');
    })


    //    show password
    $('[data-show-password]').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
            $(this).closest('.input-wrap').find('input').attr('type', 'text');
        } else {
            $(this).closest('.input-wrap').find('input').attr('type', 'password');
        }
    });

    $('[type="password"]').on('input keyup change', function () {
        var lenght = $(this).val().length;
        var container = $(this).closest('.input-wrap');

        if (lenght >= 1 && lenght <= 4) {
            container.addClass('invalid');
        } else if (lenght == 0 || lenght >= 5) {
            container.removeClass('invalid');
        }
    });



    $('[data-promo-item] input[type="text"]').on('input keyup change', function () {
        var lenght = $(this).val().length;
        var container = $(this).closest('[data-promo-item]');

        if (lenght >= 1 && lenght < 4) {
            container.find('.input-error').slideDown(0);
            container.find('.apply-wrap').css('display', 'none');
        } else if (lenght >= 5) {
            container.find('.input-error').slideUp(0);
            container.find('.apply-wrap').css('display', 'flex');
        } else if (lenght == 0) {
            container.find('.input-error').slideUp(0);
            container.find('.apply-wrap').css('display', 'none');
        }
    });


    /*hover link basket*/
    if (winWidth > 991) {
        $('[data-basket-link], .cart-popover').mouseover(function () {
            $('.cart-popover').stop().fadeIn();
        });
        $('.cart-popover, [data-basket-link]').mouseleave(function () {
            $('.cart-popover').stop().fadeOut();
        })

        $('.size-group .disabled, .card-size .popover').mouseover(function (e) {
            e.preventDefault();
            $('.card-size .popover').stop().fadeIn();
        });
        $('.size-group .disabled, .card-size .popover').mouseleave(function () {
            $('.card-size .popover').stop().fadeOut();
        });
    } else {
        $('.size-group .disabled').on('click', function (e) {
            e.preventDefault();
            $('.card-size .popover').stop().fadeToggle();
        });

        $(document).on('click', function (e) {
            if (!e.target.closest('.size-radio.disabled')) {
                $('.card-size .popover').stop().fadeOut();
            }
        });
    }

    $('.sign-form input').on('input keyup change', function () {
        var btn = $(this).closest('form').find('button');
        var count = $(this).closest('form').find('.input-wrap input').length;
        var value = $(this).closest('form').find('.value').length;

        if (count == value) {
            btn.removeClass('disabled');
            btn.removeAttr('disabled');
        } else {
            btn.addClass('disabled');
            btn.attr('disabled', 'disabled');
        }
    });

    $('.add-favorite[data-open-popover]').on('click', function (e) {
        e.preventDefault();
        var wrap = $(this).closest('[data-popover-wrap]');

        var popover = wrap.find('.popover');
        $(this).addClass('active');
        $('.popover').stop().fadeOut();
        popover.stop().fadeIn();
    })
    $('[data-favorite-remove]').on('click', function (e) {
        e.preventDefault();
        var wrap = $(this).closest('[data-popover-wrap]');
        wrap.remove();
    });

    $('[data-popover-close]').on('click', function (e) {
        e.preventDefault();
        var popover = $(this).closest('.popover');
        popover.stop().fadeOut();
    });


    if (winWidth <= 767) {
        $('.faq-row-list .text-15').on('click', function () {
            $(this).toggleClass('active');
            $(this).closest('.item').find('.fi-content').slideToggle();
        });
    } else {
        if ($('[data-sticky]').length) {
            $(function () {
                var sticky = $('[data-sticky]');
                var topPos = sticky.offset().top,
                    width = sticky.outerWidth();
                $(window).scroll(function () {
                    var top = $(document).scrollTop(),
                        pip = $('[data-sticky-scroll]').offset().top + $('[data-sticky-scroll]').outerHeight(),
                        height = sticky.outerHeight();
                    if (top > topPos && top < pip - height) {
                        sticky.addClass('fixed').removeAttr("style");
                        sticky.css('width', width);
                    } else if (top > pip - height) {
                        sticky.removeClass('fixed').css({
                            'position': 'absolute',
                            'bottom': '0'
                        });
                    } else {
                        sticky.removeClass('fixed');
                    }
                });
            });
        }
    }

    if (winWidth >= 992) {
        $('.order--td').on('click', function () {
            var container = $(this).closest('.order-wrap');
            var height = container.find('.order-detail').css('height');
            var detail = container.find('.order-detail');
            if (!container.hasClass('active')) {
                $('.order-wrap').removeClass('active');
                container.addClass('active');
                $('.tr-hide').css('display', 'none');
                container.find('.tr-hide').css('display', 'flex');
                $('.orders-body').css('min-height', height);
                $('.order-detail').stop().fadeOut(350);
                detail.stop().fadeIn(350);
            } else {
                $('.order-detail').stop().fadeOut(350);
                $('.order-wrap').removeClass('active');
                $('.orders-body').css('min-height', '200px');
                container.find('.tr-hide').css('display', 'none');
            }
        });
    } else {
        $('.order-wrap').on('click', function () {
            var container = $(this);
            var detail = container.find('.order-detail');
            if (!container.hasClass('active')) {
                $('.order-wrap').removeClass('active');
                container.addClass('active');
                $('.order-detail').stop().slideUp(350);
                detail.stop().slideDown(350);
            } else {
                $('.order-detail').stop().slideUp(350);
                $('.order-wrap').removeClass('active');
            }
        });

    }
    var height = $('.site-header').outerHeight();
    $('.margin-header').css('height', height);

    $('.help-link').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.help').toggleClass('active');
    })

    setTimeout(function () {
        $('.filters').addClass('active');
    }, 1000);


    // searc form
    $('.form-search input').on('input keyup change', function () {
        var lenght = $(this).val().length;
        var container = $(this).closest('.form-search');

        if (lenght >= 1) {
            container.addClass('typed');
        } else if (lenght == 0) {
            container.removeClass('typed');
        }
    });
    $('.form-search .clear-search, .form-search-close').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('.form-search'),
            form = container.find('form');
        container.removeClass('typed');
        form.trigger('reset');
        container.find('.search-wrap').removeClass('active');
    });

    $('.filter--title').on('click', function () {
        var item = $(this).closest('.filter--item');

        item.toggleClass('active');
        item.find('.filter--item-content').slideToggle();

    });

    $('.input-number').bind("change keyup input click", function () {
        var $this = $(this);

        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });

    $('.open-order-product').click(function (e) {
        e.preventDefault();

        var text = $(this).attr('data-text');
        var textActive = $(this).attr('data-text-active');

        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).text(textActive);
            $('.checkout-info .mc-tr.hide').css('display', 'flex');
        } else {
            $(this).removeClass('active');
            $(this).text(text);
            $('.checkout-info .mc-tr.hide').css('display', 'none');
        }
    });

    $('.mf-item .row-title').click(function () {
        if (!$(this).hasClass('active')) {
            $('.row-title').removeClass('active');
            $('.mf-item').find('.mf-item-content').stop().slideUp();
            $(this).addClass('active');
            $(this).closest('.mf-item').find('.mf-item-content').stop().slideDown();
        } else {
            $('.row-title').removeClass('active');
            $('.mf-item').find('.mf-item-content').stop().slideUp();
        }
    });
    $(document).on('click', function (e) {
        if (!e.target.closest('.mf-item')) {
            $('.row-title').removeClass('active');
            $('.mf-item').find('.mf-item-content').stop().slideUp();
        }
    });

});
