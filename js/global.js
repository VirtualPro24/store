var _functions = {}, winWidth;

jQuery(function ($) {

  "use strict";

  /* function on page ready */
  var isTouchScreen = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);
  if (isTouchScreen) $('html').addClass('touch-screen');
  var winScr, winHeight,
    is_Mac = navigator.platform.toUpperCase().indexOf('MAC') >= 0,
    is_IE = /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/\d+/.test(navigator.userAgent),
    is_Chrome = navigator.userAgent.indexOf('Chrome') >= 0 && navigator.userAgent.indexOf('Edge') < 0;

  /* check browser */
  winWidth = $(window).width();
  winHeight = $(window).height();

  if (is_Mac) { $('html').addClass('mac'); }
  if (is_IE) { $('html').addClass('ie'); }
  if (is_Chrome) { $('html').addClass('chrome'); }

  /* page loader and etc */
  if ($('.select-box').length) {
    $('.SelectBox').SumoSelect();
  }

  scrollSpy();

  /* function on page scroll */
  $(window).scroll(function () {
    _functions.scrollCall();
    scrollSpy();
  });

  _functions.scrollCall = function () {
    winScr = $(window).scrollTop();
    if (winScr > 10) {
      $('header').addClass('scrolled');
    } else {
      $('header').removeClass('scrolled');
    }

    var navHeight = $('.top-section').height();
    if ($(window).scrollTop() > navHeight) {
      $('.anchors-nav').addClass('fixed');
    } else {
      $('.anchors-nav').removeClass('fixed');
    }
  }

  /*menu*/
  $(document).on('click', '.mobile-button', function () {
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $('.dropmenu').addClass('show');
      _functions.removeScroll();
    } else {
      $(this).removeClass('active');
      $('.dropmenu').removeClass('show');
      _functions.addScroll();
    }
  });

  //layer close
  $('.layer-close').on('click', function () {
    $('header').removeClass('active-layer-close');
    $('.user-account').removeClass('open');
    $('.menu-notification').removeClass('open');
  });

  //open-search-form
  $('.open-search-form').on('click', function () {
    $(this).parents('.search-block').addClass('active');
    $('.mobile-button').removeClass('active');
    $('html').removeClass('overflow-menu');
    $('.toggle-block').removeClass('open');
    $(this).parents('.search-block').find('.search-form .input').focus();
  });

  //closed-search-form
  $('.search-block .close-search').on('click', function () {
    $(this).parents('.search-block').removeClass('active');
  });

  //custom-input-number
  $('.custom-input-number .increment').on('click', function () {
    var $input = $(this).siblings('.input'),
      val = parseInt($input.val()),
      max = parseInt($input.attr('max')),
      step = parseInt($input.attr('step'));
    var temp = val + step;
    $input.val(temp <= max ? temp : max);
  });

  $('.custom-input-number .decrement').on('click', function () {
    var $input = $(this).siblings('.input'),
      val = parseInt($input.val()),
      min = parseInt($input.attr('min')),
      step = parseInt($input.attr('step'));
    var temp = val - step;
    $input.val(temp >= min ? temp : min);
  });

  //focus input
  $('.input-field-wrapp .input').on('focus', function () {
    $(this).closest('.input-field-wrapp').addClass('focus');
  });
  $('.input-field-wrapp .input').on('blur', function () {
    $(this).closest('.input-field-wrapp').removeClass('focus');
  });
  $('.input-field-wrapp .input').on('keyup', function () {
    if ($(this).val()) $(this).parent().addClass('value');
    else $(this).parent().removeClass('value');
  });
  $('.input-field-wrapp select').on('change', function () {
    $(this).parent().addClass('value');
  });

  //fail Input
  $('.input').on('keyup', function () {
    if ($(this).val()) {
      $(this).parent('.input-field-wrapp').removeClass('fail');
    }
    else { $(this).parent('.input-field-wrapp').addClass('fail'); }
  });

  //telephone mask
  $('.input[type="tel"]').on('focus', function () {
    $(this).inputmask({ 
      "mask": "+380-99-999-9999",
      showMaskOnHover: false,
    });
  });

  /* swiper sliders */
  _functions.getSwOptions = function (swiper) {
    var options = swiper.data('options');
    options = (!options || typeof options !== 'object') ? {} : options;
    var $p = swiper.closest('.swiper-entry'),
      slidesLength = swiper.find('>.swiper-wrapper>.swiper-slide').length;
    if (!options.pagination) options.pagination = {
      el: $p.find('.swiper-pagination')[0],
      clickable: true
    };
    if (!options.navigation) options.navigation = {
      nextEl: $p.find('.swiper-button-next')[0],
      prevEl: $p.find('.swiper-button-prev')[0]
    };
    options.preloadImages = false;
    options.lazy = { loadPrevNext: true };
    options.observer = true;
    options.observeParents = true;
    options.watchOverflow = true;
    if (!options.speed) options.speed = 500;
    options.roundLengths = false;
    if (!options.centerInsufficientSlides) options.centerInsufficientSlides = false;
    if (options.customFraction) {
      $p.addClass('custom-fraction-swiper');
      if (slidesLength > 1 && slidesLength < 10) {
        $p.find('.custom-current').text('01');
        $p.find('.custom-total').text('0' + slidesLength);
      } else if (slidesLength > 1) {
        $p.find('.custom-current').text('01');
        $p.find('.custom-total').text(slidesLength);
      }
    }
    if (isTouchScreen) options.direction = "horizontal";

    if (options.initialLast) options.initialSlide = slidesLength;

    if (options.customPagination) {
      options.pagination.renderBullet = function (index, className) {
        var slide = swiper.find('.swiper-slide').eq(index);
        if (slide.data('video')) className += ' video';
        return '<span class="' + className + ' custom" style="background-image: url(' + slide.data('preview') + ')"><span></span></span>';
      }
    }

    if (options.progressbar) {
      options.pagination.type = 'progressbar';
    }
    if (options.customFraction) {
      $p.addClass('custom-fraction-swiper');
      if (slidesLength > 1 && slidesLength < 10) {
        $p.find('.custom-current').text('01');
        $p.find('.custom-total').text('0' + slidesLength);
      } else if (slidesLength > 1) {
        $p.find('.custom-current').text('01');
        $p.find('.custom-total').text(slidesLength);
      }
    }
    return options;
  };

  _functions.initSwiper = function (el) {
    var swiper = new Swiper(el[0], _functions.getSwOptions(el));
  };

  $('.swiper-entry .swiper-container').each(function () {
    _functions.initSwiper($(this));
  });

  $('.swiper-thumbs').each(function () {
    var top = $(this).find('.swiper-container.swiper-thumbs-top')[0].swiper,
      bottom = $(this).find('.swiper-container.swiper-thumbs-bottom')[0].swiper;
    top.thumbs.swiper = bottom;
    top.thumbs.init();
    top.thumbs.update();
  });

  $('.swiper-control').each(function () {
    var top = $(this).find('.swiper-container')[0].swiper,
      bottom = $(this).find('.swiper-container')[1].swiper;
    top.controller.control = bottom;
    bottom.controller.control = top;
  });
  //custom fraction
  $('.custom-fraction-swiper').each(function () {
    var $this = $(this),
      $thisSwiper = $this.find('.swiper-container')[0].swiper;

    $thisSwiper.on('slideChange', function () {
      $this.find('.custom-current').text(
        function () {
          if ($thisSwiper.realIndex < 9) {
            return '0' + ($thisSwiper.realIndex + 1)
          } else {
            return $thisSwiper.realIndex + 1
          }
        }
      )
    });
  });

  //popup
  var popupTop = 0;
  _functions.removeScroll = function () {
    popupTop = $(window).scrollTop();
    $('html').css({
      "position": "fixed",
      "top": -$(window).scrollTop(),
      "width": "100%"
    });
  }
  _functions.addScroll = function () {
    $('html').css({
      "position": "static"
    });
    window.scroll(0, popupTop);
  }
  _functions.openPopup = function (popup) {
    $('.popup-content').removeClass('active');
    $(popup + ', .popup-wrapper').addClass('active');
    _functions.removeScroll();
  };

  _functions.closePopup = function () {
    $('.popup-wrapper, .popup-content').removeClass('active');
    setTimeout(function () {
      $('.popup-content[data-rel="video-popup"] .popup-video-wrapper').html('');
    }, 300);
    _functions.addScroll();
  };

  _functions.textPopup = function (title, description) {
    $('#text-popup .text-popup-title').html(title);
    $('#text-popup .text-popup-description').html(description);
    _functions.openPopup('#text-popup');
  };

  $(document).on('click', '.open-popup', function (e) {
    e.preventDefault();
    _functions.openPopup('.popup-content[data-rel="' + $(this).data('rel') + '"]');
  });

  $(document).on('click', '.popup-wrapper .btn-close, .popup-wrapper .layer-close, .popup-wrapper .close-popup', function (e) {
    e.preventDefault();
    _functions.closePopup();
  });

  //close popup with ESCAPE key
  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      _functions.closePopup();
    }
  });

  // Open video popup
  $(document).on('click', '.open-video', function () {
    var video = '<iframe class="video" src="' + $(this).data('video') + '" style="width: 100%; height: 100%;" frameborder="0" allow="autoplay" allowfullscreen></iframe>';
    //var video = '<iframe src="https://players.brightcove.net/'+ $(this).data("brightcore-account") + '/'+ $(this).data('player') +'_default/index.html?videoId=' + $(this).data("video-id") + '&autoplay=true" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" width="100%" height="100%"></iframe>';
    $('.popup-content[data-rel="' + $(this).data('rel') + '"] .popup-video-wrapper').html(video);
    _functions.openPopup('.popup-content[data-rel="' + $(this).data('rel') + '"]');
    return false;
  });

  function scrollSpy() {

    if ($('.anchors').length) {
      var section = $('.anchor-sect'),
        sections = {},
        i = 0,
        scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

      Array.prototype.forEach.call(section, function (e) {
        sections[e.id] = e.offsetTop;
      });

      for (i in sections) {
        if (sections[i] <= (scrollPosition - 300)) {
          $('.anchors-nav a.active').attr('class', '');
          $('.anchors-nav a[href*=' + i + ']').attr('class', 'active');
        }
      }
    }
  }

  //anchor scroll
  $(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - $('header').outerHeight() - $('.anchors-nav').outerHeight()
          }, 1000);
          return false;
        }
      }
    });
  });

  //accordion
  $(document).on('click', '.accordeon-item:not(.edit) .accordeon-title', function () {
    if ($(this).hasClass('active')) {
      $(this).parent('.accordeon-item').removeClass('active').siblings('.accordeon-item').addClass('active');
      $(this).removeClass('active').next().slideUp();
    }
    else {
      $(this).closest('.accordeon').find('.accordeon-title').not(this).removeClass('active').next().slideUp();
      $(this).addClass('active').next().slideDown();
      $(this).parent('.accordeon-item').addClass('active').siblings('.accordeon-item').removeClass('active');
    }
  });
  $(document).on('click', '.next-acc', function(){
    // hide current item
    $(this).closest('.accordeon-item').removeClass('active');
    $(this).closest('.accordeon-item').find('.accordeon-title').removeClass('active');
    $(this).closest('.accordeon-item').find('.accordeon-content').slideUp();

    // show next item
    $(this).closest('.accordeon-item').next('.accordeon-item').addClass('active');
    $(this).closest('.accordeon-item').next('.accordeon-item').find('.accordeon-title').addClass('active');
    $(this).closest('.accordeon-item').next('.accordeon-item').find('.accordeon-content').slideDown();
  });
  $(document).on('click', '.prev-acc', function(){
    // hide current item
    $(this).closest('.accordeon-item').removeClass('active');
    $(this).closest('.accordeon-item').find('.accordeon-title').removeClass('active');
    $(this).closest('.accordeon-item').find('.accordeon-content').slideUp();

    // show next item
    $(this).closest('.accordeon-item').prev('.accordeon-item').addClass('active');
    $(this).closest('.accordeon-item').prev('.accordeon-item').find('.accordeon-title').addClass('active');
    $(this).closest('.accordeon-item').prev('.accordeon-item').find('.accordeon-content').slideDown();
  });

  //get-starter-form
  $(document).on('change', '.get-starter-form .checkbox-item input', function () {
    if ($(this).is(':checked')) {
      $(this).prop("checked", true);
      $(this).closest('.get-starter-form').find('.btn').removeClass('btn-disabled')
    }
    else $(this).closest('.get-starter-form').find('.btn').addClass('btn-disabled')
  });

  //open panel with add product panel
  $(document).on('click', '.add-product-block .add-product', function () {
    $(this).closest('.add-product-block').toggleClass('open-panel')
  });
  $(document).on('click', '.empty-project-block .empty-text .add-product', function () {
    $('.add-product-block').toggleClass('open-panel')
  });
  $(document).on('click', '.layer-close, .add-product-inner ul li', function () {
    $('.add-product-block').removeClass('open-panel')
  });

  //added-products
  $(document).on('click', '.control-btn .remove', function () {
    $(this).parents('tr').remove()
  });

  //custom select
  $(document).on('click', function (e) {
    $('.с-select-wrapp').removeClass('open-select');
  });
  $(document).on('click', '.с-select .с-current-val', function (e) {
    e.stopPropagation();
    $('.с-select-wrapp').removeClass('open-select');
    $(this).parents('.с-select-wrapp').toggleClass('open-select');
  });

  $(document).on('click', '.с-select ul li', function () {
    let thisValue = $(this).text(),
      thisIndex = $(this).index();

    $(this).closest('ul').find('> .active').removeClass('active');
    $(this).addClass('active');
    $(this).parents('.с-select-wrapp').addClass('value');
    $(this).parents('.с-select-wrapp').removeClass('open-select');
    $(this).parents('.с-select').find('.с-current-val').html(thisValue);

    if ($(this).parents('.inner-middle-popup').hasClass('contains-hide-block')) {
      $(this).parents('.inner-middle-popup').find('.hide-block').slideDown()
      $(this).parents('.popup-align').find('.inner-bottom-popup .btn-disabled').removeClass('btn-disabled')
    }

    if($('.awards').length > 0){
      let thisSwiper = $('.awards').find('.swiper-container')[0].swiper;
      $('.awards .swiper-slide').each(function () {
        let slideIndex = $(this).index();
        if (thisIndex == slideIndex) {
          thisSwiper.slideTo(slideIndex);
        }
      });
    }

  });

  //vosible/hide hint text
  $(document).on('click', '.visible-hint', function () {
    $(this).parents().find('.hint-inner').slideToggle();
  });

  //remove one instalation item
  $('.list-calculate-item .control-btn .remove').on('click', function () {
    $(this).parents('.list-calculate-item').remove();
  });

  //remove all instalation item
  $('.calculate-instalation-area .remove-all').on('click', function () {
    $(this).parents('.calculate-instalation-area').find('.list-calculate').remove();
  });

  //check border orientation
  $('.border-orientation .orientation-item').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active')
    $(this).siblings().find('input').prop("checked", false);
    $(this).find('input').prop("checked", true);
  });

  //switch calculate
  $('.switch-calculate .caption').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active')
    let calculateItem = $(this).data('caption');
    $('.switch-calculate-inner .switch-calculate-item').each(function () {
      if ($(this).data('inner') == calculateItem) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    });
  });

  //save project
  $('.save-btn').on('click', function () {
    setTimeout(function () {
      $('.save-project').addClass('active');
    }, 300);
    setTimeout(function () {
      $('.save-project').removeClass('active');
    }, 2000);
  });

  //visible control buttons
  if ($(window).width() < 1200) {
    $('.added-products .control-btn').on('click', function () {
      $(this).toggleClass('active')
    });
    $(document).on('click', '.control-btn.active .layer-close', function () {
      $(this).parents('.control-btn').removeClass('active')
    });
  }

  //colapse table
  $('.table-caption').on('click', function () {
    $(this).toggleClass('active')
    $(this).parent().find('.inner-colapse').slideToggle()
  });

  //edit name project
  $('.popup-content .save-name').on('click', function () {
    let getNewName = $(this).closest('.popup-content').find('#edit-name').val()
    $('.edit-title').find('span').text(getNewName)
  });

  //scroll top
  $(document).on('click', '.scroll-top', function () {
    $('body, html').animate({
      'scrollTop': 0
    }, 800);
  });

  //scroll animation
  function scrollAnime() {
    if ($('.animate-item').length && !is_IE) {
      $('.animate-item').not('.animated').each(function () {
        var th = $(this);
        if ($(window).scrollTop() >= th.offset().top - ($(window).height() * 0.9)) {
          th.addClass('animated');
        }
      });
    }
  }
  scrollAnime();
  $(window).on('scroll', function () {
    scrollAnime();
  });

  $('.megamenu-left ul li a').on('mouseover', function () {
    var i = $(this).closest('ul').find('a').index(this);
    var image = $(this).closest('.megamenu').find('.megamenu-img');
    image.removeClass('active')
    image.eq(i).addClass('active');
  });

  $('.megamenu-left').on('mouseleave', function () {
    $(this).closest('.megamenu').find('.megamenu-img').removeClass('active');
  });

  /* lang */
  if (isTouchScreen) {
    $(document).on('click', function (e) {
      $('.header-lang').removeClass('active');
    });
    $(document).on('click', '.header-lang-current', function (e) {
      e.stopPropagation();
      $(this).closest('.header-lang').addClass('active');
    });
  }

  /* drop arrow */
  $(document).on('click', '.dropmenu-arrow', function () {
    $(this).toggleClass('active');
    $(this).next('ul').slideToggle();
  });

  _functions.pageScroll = function (current, header_height, spacer) {
    $('html, body').animate({ scrollTop: current.offset().top - header_height - spacer }, 700);
  }

  $(document).on('click', '.scroll-to', function () {
    _functions.pageScroll($('#support'), 80, $('.spacer-sect').outerHeight());
  });

  /* upload file */
  $(document).on('click', '.upload-wrapper .file-name', function () {
    $(this).closest('.upload-wrapper').find('input').click();
  });
  $(document).on('change', '.upload-wrapper input', function () {
    var fileName = $(this).val().substring($(this).val().lastIndexOf("\\") + 1);
    if (!fileName) {
      $(this).closest('.upload-wrapper').find('.remove-file').click();
    } else {
      $(this).parent().addClass('active');
      $(this).closest('.file-form').find('.uploaded-files').append('<div class="uploaded-item d-flex align-items-center"><span>' + fileName + '</span><div class="remove-file"></div></div>');
      $(this).closest('.file-form').find('.file-name').addClass('active');
    }
  });

  /* remove file */
  $(document).on('click', '.remove-file', function () {
    $(this).parent('.uploaded-item').remove();
  });

  /* show articles */
  $(document).on('click', '.show-articles', function(){
    $('.articles [class*="col-"]').each(function(){
      if($(this).hasClass('article-hidden')){
        $(this).fadeIn();
        $(this).removeClass('article-hidden');
      };      
    });    
  });

  /* tabs */
  $('.tab-title').on('click', function() {
    $(this).parent().toggleClass('active');
  });
  $('.tab-toggle div').on('click', function() {
    var tab = $(this).closest('.tabs').find('.tab');
    var i = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    tab.eq(i).siblings('.tab:visible').fadeOut(function() {
        tab.eq(i).fadeIn();
    });
    $(this).closest('.tab-nav').removeClass('active').find('.tab-title').text($(this).text());
  });

  /* search */
  $(document).on('click', '.open-search-popup', function(){
    $('.search-popup').find('input[type="text"]').focus();
    $('.search-popup').addClass('show');
    if($('.dropmenu').hasClass('show')) $('.dropmenu').removeClass('show');
    if($('.mobile-button').hasClass('active')) $('.mobile-button').removeClass('active');
  });
  $(document).on('click', '.search-popup-close', function(){
    $('.search-popup').removeClass('show');
  });

  /* choose-contractor */
  $(document).on('click', function(e){
    $('.header-submenu').removeClass('active');
  });
  $(document).on('click', '.choose-contractor', function(e){
    e.stopPropagation();
    $(this).closest('.header-submenu').toggleClass('active');
  });

  /* email remove */
  $(document).on('click', '.email-selected', function(){
    $(this).remove();
  });

    $('.sf-title').on('click', function(){
        $(this).parent().toggleClass('active');
        $(this).parent('.side-filter').find('.sf-list').slideToggle();
    });    
    
    $('.sidebar-header').on('click', function(){
        $(this).parent().toggleClass('active');
        $(this).parent().find('.sidebar-content').slideToggle(400);
        
        if($(this).parent().hasClass('active')){
            $(this).find('.sidebar-toggle').text('Open filter');
            $('.content-right').addClass('active');
        } else {
            $(this).find('.sidebar-toggle').text('Hide filter');
            $('.content-right').removeClass('active');
        }
    });
    
    
    var countFilter = 0;
    
    $('.sf-checkbox input').change(function(){
        if($(this).is(":checked")){
            countFilter++;
        } else {
            countFilter--;
        }
        
        $('.sidebar-info p').text(countFilter + ' selected');
        if(countFilter >=1 ){
            $('.sidebar-info').slideDown();
        } else {
            $('.sidebar-info').slideUp();
        }
    });
    
    $('.sidebar-info .clear-btn').click(function(e){
        e.preventDefault();
        $('.sidebar').trigger('reset');
        countFilter = 0;
        $('.sidebar-info p').text(countFilter + ' selected');
        $('.sidebar-info').slideUp();
    })    
    $('.product-item.more').click(function(e){
        e.preventDefault();
         
        $(this).closest('.products-block').find('.product-item').slideDown();    
        $(this).slideUp();
    });
    
    $('.search-clear').on('click', function(e){
        e.preventDefault();
        
        $(this).closest('form').trigger('reset');
        $(this).closest('.input-field-wrapp').removeClass('value');
    })
    
});
