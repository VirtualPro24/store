<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Categories</title>
</head>

<body class="home-page">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header bg-1"></div>

        <div class="categories-top bg-1">
            <div class="container">
                <div class="breadcrumbs hidden-xs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Жіноче взуття</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>
                <a href="" class="mobile-back only-mob">Жіноче взуття</a>
                <h1 class="page-title h2">Туфлі</h1>
                <div class="page-title h2 hidden-xs">Жіноче взуття</div>

                <form class="filters">
                    <div class="products-count">336 Товарів</div>
                    <div class="mobile-filters row" style="opacity: 0;">
                        <div class="filter-close"></div>

                        <div class="mobile-filters-title">Фільтрувати</div>
                        <div class="mf-item">
                            <div class="row-title">Ціна</div>
                            <div class="mf-item-content">
                                <div class="price-row">
                                    <div class="input-wrap">
                                        <label class="input-title" for="from">Від</label>
                                        <input type="text" class="input input-number" id="from">
                                    </div>
                                    <span>-</span>
                                    <div class="input-wrap">
                                        <label class="input-title" for="to">До</label>
                                        <input type="text" class="input input-number" id="to">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mf-item">
                            <div class="row-title">Розмір</div>
                            <div class="mf-item-content">
                                <div class="card-size">
                                    <div class="size-group">
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="35"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="36"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="37"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="38"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="39"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="40"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="41"></span>
                                        </label>
                                        <label class="size-radio">
                                            <input type="checkbox" hidden="" name="size">
                                            <span class="size-radio-count" data-text="42"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-box">
                            <select class="SelectBox" name="" multiple>
                                <option value="" selected disabled>Вид</option>
                                <option value="">Вид 1</option>
                                <option value="">Вид 2</option>
                                <option value="">Вид 3</option>
                                <option value="">Вид 4</option>
                                <option value="">Вид 5</option>
                                <option value="">Вид 6</option>
                            </select>
                        </div>
                        <div class="select-box">
                            <select class="SelectBox" name="" multiple>
                                <option value="" selected disabled>Колір</option>
                                <option value="" class="color red">Червоний</option>
                                <option value="" class="color white">Былий</option>
                                <option value="" class="color yellow">Жовтий</option>
                                <option value="" class="color green">Зелений</option>
                                <option value="" class="color blue">Блакитний</option>
                                <option value="" class="color black">Чорний</option>
                            </select>
                        </div>
                        <div class="select-box">
                            <select class="SelectBox" name="" multiple>
                                <option value="" selected disabled>Розмір</option>
                                <option value="">Розмір 1</option>
                                <option value="">Розмір 2</option>
                            </select>
                        </div>
                        <div class="select-box">
                            <select class="SelectBox" name="">
                                <option value="" selected disabled>Стиль</option>
                                <option value="">Стиль 1</option>
                                <option value="">Стиль 2</option>
                            </select>
                        </div>
                        <div class="select-box ">
                            <select class="SelectBox" name="">
                                <option value="" selected disabled>Матеріал</option>
                                <option value="">Матеріал 1</option>
                                <option value="">Матеріал 2</option>
                            </select>
                        </div>
                        <div class="select-box ">
                            <select class="SelectBox" name="">
                                <option value="" selected disabled>Бренди</option>
                                <option value="">Бренди 1</option>
                                <option value="">Бренди 2</option>
                            </select>
                        </div>
                        <div class="select-box select-hide">
                            <select class="SelectBox" name="">
                                <option value="" selected disabled>Матеріал</option>
                                <option value="">Матеріал 1</option>
                                <option value="">Матеріал 2</option>
                            </select>
                        </div>
                        <div class="select-box select-hide">
                            <select class="SelectBox" name="">
                                <option value="" selected disabled>Бренди</option>
                                <option value="">Бренди 1</option>
                                <option value="">Бренди 2</option>
                            </select>
                        </div>
                    </div>

                    <div class="filters-bottom">
                        <button class="btn btn-stroke hidden-xs">Застосувати</button>
                        <a href="" class="filter-more" data-text="Більше фільтрів" data-text-active="Менше фільтрів">Більше фільтрів</a>
                    </div>
                </form>
            </div>
        </div>

        <div class="products-section">
            <div class="container">
                <div class="products-header">
                    <div class="sort">
                        <div class="sort-text">Сортувати:</div>
                        <div class="select-box select-box-line">
                            <select class="SelectBox" name="">
                                <option value="" selected>За цiною</option>
                                <option value="">За зростанням ціни</option>
                            </select>
                        </div>
                    </div>
                    <a href="" class="mobile-filter">
                        Фільтрувати
                    </a>
                    <div class="filter-selected">
                        <ul>
                            <li>Чоботи <span class="icon"></span></li>
                            <li>Розмiр:40 <span class="icon"></span></li>
                            <li>Сезон:Весна <span class="icon"></span></li>
                        </ul>

                        <a href="" class="filter-remove">Скинути фiльтри</a>
                    </div>

                </div>

                <div class="product-list">
                    <div class="product" itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite active"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- PAGINATION -->
                <section class="list-show-options">
                    <a href="" class="load-more">Показати ще 35</a>
                    <? include '_pagination.php';?>

                </section>

                <div class="breadcrumbs only-mob">
                    <ul>
                        <li><a href="index.php">Головна</a></li>
                        <li><a href="categories.php">Жіноче взуття</a></li>
                        <li><span>LE'BERDES черевики осінні</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- store CARDS -->
        <section>
            <? include '_features.php';?>
        </section>

        <!-- store CARDS -->
        <section>
            <? include '_store_cards.php';?>
        </section>

        <div class="seo-block hidden-xs bg-1">
            <div class="container">
                <h2 class="h3">Купити взуття вигідно та по-сучасному в інтернет-магазині favorite shoes у Києві, Львові та інших містах України</h2>
                <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                    З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>

                <div class="seo-hidden">
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                </div>
                <div class="seo-arrow"><img src="img/seo-arrow.svg" alt=""></div>
            </div>
        </div>

    </div>


    <!-- SEO TAG -->
    <section class="bg-1">
        <? include '_seo_tag.php';?>
    </section>

    <!-- FAQ -->
    <section class="bg-1">
        <? include '_FAQ.php';?>
    </section>

    <!-- SUBSCRIBE -->
    <section>
        <? include '_subscribe.php';?>
    </section>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <div class="filter--mob">
        <div class="filter--content selected">
            <div class="filter--header">
                <div class="title">Фільтрувати</div>
                <div class="close--filter"><img src="img/icon-cross.svg" alt=""></div>
            </div>
            <div class="filter--middle">
                <div class="filter--item">
                    <div class="filter--title">Ціна</div>
                    <div class="filter--item-content">
                        <div class="price-row">
                            <div class="input-wrap">
                                <label class="input-title" for="from">Від</label>
                                <input type="text" class="input input-number" id="from">
                            </div>
                            <span>-</span>
                            <div class="input-wrap">
                                <label class="input-title" for="to">До</label>
                                <input type="text" class="input input-number" id="to">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="filter--item">
                    <div class="filter--title">Вид</div>
                    <div class="filter--item-content">
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Босоніжки</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Еспадрильї</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Кеди </span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Кросівки</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Лофери</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Мокасини</span>
                        </label>
                    </div>
                </div>
                <div class="filter--item">
                    <div class="filter--title">Розмір</div>
                    <div class="filter--item-content">

                        <div class="card-size">
                            <div class="size-group">
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="35"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="36"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="37"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="38"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="39"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="40"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="41"></span>
                                </label>
                                <label class="size-radio">
                                    <input type="checkbox" hidden="" name="size">
                                    <span class="size-radio-count" data-text="42"></span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="filter--item">
                    <div class="filter--title">Стиль</div>
                    <div class="filter--item-content">
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Босоніжки</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Еспадрильї</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Кеди </span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Кросівки</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Лофери</span>
                        </label>
                        <label class="checkbox-item">
                            <input type="checkbox" hidden="">
                            <span class="checkbox-check">Мокасини</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="filter--bottom">
                <div class="filter-selected">
                    <ul>
                        <li>Чоботи <span class="icon"></span></li>
                        <li>Розмiр:40 <span class="icon"></span></li>
                        <li>Сезон:Весна <span class="icon"></span></li>
                    </ul>

                </div>
                <div class="btn-group">
                    <a href="" class="btn btn-primary">Застосувати</a>
                    <a href="" class="btn btn-stroke">Скинути фiльтри</a>

                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
