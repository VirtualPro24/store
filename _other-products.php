<div class="other-products">
    <div class="spacer-120"></div>
    <div class="container swiper-entry">
        <div class="block-title">Вам також може сподобатись</div>
        <div class="other-cards-slider">
            <div class="other-cards-options">
                <div class="swiper-pagination"></div>
                <div class="swiper-button-group type-2">
                    <div class="swiper-button-prev button type-2"><img src="img/arrow-long-right.svg" alt=""></div>
                    <div class="swiper-button-next button type-2"><img src="img/arrow-long-right.svg" alt=""></div>
                </div>
            </div>
            <div class="swiper-container" data-options='{"slidesPerView":4, "loop":false,"spaceBetween":35, "breakpoints":{"991": {"slidesPerView":3,"loop":false,"spaceBetween":16}, "575": {"slidesPerView":1,  "loop":true, "spaceBetween":16}}}'>
                <div class="swiper-wrapper">
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">3249 ₴</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod-2.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">₴ 3249</span>
                                <span class="old">₴ 4249</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">3249 ₴</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod-2.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">3249 ₴</span>
                                <span class="old">4249 ₴</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">3249 ₴</span>
                                <span class="old">4249 ₴</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card swiper-slide">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/other-prod-2.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">LE'BERDES сумка</div>
                            <div class="price type-2">
                                <span class="new">3249 ₴</span>
                                <span class="old">4249 ₴</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
        </div>
    </div>
    <div class="spacer-120"></div>
    <div class="breadcrumbs only-mob">
        <ul>
            <li><a href="index.php">Головна</a></li>
            <li><a href="categories.php">Жіноче взуття</a></li>
            <li><span>LE'BERDES черевики осінні</span></li>
        </ul>
    </div>
</div>
