<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Увiйти</title>
</head>

<body class="home-page">

    <div id="content-block" class="bg-1">

        <div class="sign page">
            <div class="sign-header">
                <a href="categories.php" class="link-back">Назад в магазин</a>
                <a href="" class="logo only-mob">
                    <img src="img/logo.svg" alt="">
                </a>                
            </div>
            <div class="sign-page-content">
                <div class="sign-row">
                    <div class="sign-image sign-col">
                        <img src="img/sign/sign-img.jpg" alt="">
                    </div>
                    <div class="sign-col sign-content recover-info bg-2">
                        <a href="" class="logo hidden-xs">
                            <img src="img/logo.svg" alt="">
                        </a>
                        <div class="title h2 type-2">Ми вiдправили посилання для вiдновлення паролю на вашу пошту
                            <span class="send-email">ivanov@example.com</span>
                        </div>

                        <a href="" class="btn btn-stroke type-2 btn-reload">вiдправити знову</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
