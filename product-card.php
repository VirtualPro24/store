<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Product card</title>
</head>

<body class="home-page">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>

        <div class="detail-card">
            <div class="container">
                <div class="detail-card-left">
                    <div class="breadcrumbs hidden-xs">
                        <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <a href="index.php" itemprop="item">Головна</a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <a href="index.php" itemprop="item">Жіноче взуття</a>
                                <meta itemprop="position" content="2" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <span itemprop="name">ЖLE'BERDES Черевики зимові</span>
                                <meta itemprop="position" content="3" />
                            </li>
                        </ul>
                    </div>

                    <div class="detail-card-slider swiper-entry">
                        <div class="detail-card-rate">
                            <div class="count">4.5</div>
                            <div class="rate-list">
                                <img src="img/star-brow.svg" alt="">
                                <img src="img/star-brow.svg" alt="">
                                <img src="img/star-brow.svg" alt="">
                                <img src="img/star-brow.svg" alt="">
                                <img class="star-empty" src="img/star-brow.svg" alt="">
                            </div>
                            <div class="badge-group only-mob">
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div> 
                            </div>                            
                        </div>
                        <div class="swiper-container" data-options='{"slidesPerView":2, "loop":false, "spaceBetween":0, "breakpoints":{"575": {"slidesPerView":1, "spaceBetween":10}}}'>
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a data-fancybox="card" href="img/card-image-1.jpg" class="image"><img class="lazy" data-src="img/card-image-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a data-fancybox="card" href="img/card-image-2.jpg" class="image"><img class="lazy" data-src="img/card-image-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a data-fancybox="card" href="img/card-image-1.jpg" class="image"><img class="lazy" data-src="img/card-image-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a data-fancybox="card" href="img/card-image-2.jpg" class="image"><img class="lazy" data-src="img/card-image-2.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="detail-card-bottom">
                            <div class="swiper-options">
                                <div class="swiper-button-prev button"><img src="img/arrow-long-right.svg" alt=""></div>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-next button"><img src="img/arrow-long-right.svg" alt=""></div>
                            </div>
                            <a href="" class="btn-video type-2">
                                <span class="icon">
                                    <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7 10.6813V13.348C7 16.3251 7 17.8137 7.97101 18.3963C8.94202 18.9789 10.2555 18.2784 12.8824 16.8774L15.3824 15.5441C18.2941 13.9911 19.75 13.2146 19.75 12.0146C19.75 10.8146 18.2941 10.0382 15.3824 8.48524L12.8824 7.1519C10.2555 5.75089 8.94202 5.05039 7.97101 5.633C7 6.2156 7 7.70417 7 10.6813Z" stroke="#876B46" />
                                    </svg>

                                </span>
                                посмотреть видео
                            </a>
                        </div>
                    </div>
                </div>
                <div class="detail-card-right">
                    <div class="detail-card-info">
                        <div class="badge-group">
                            <div class="badge top">Top</div>
                            <div class="badge new">New</div>
                            <div class="badge promo">Promo</div>
                        </div>
                        <div class="in-stok">
                            <img src="img/check.svg" alt="">
                            в наявності
                        </div>
                        <div class="card-title h2">LE'BERDES Черевики зимові</div>
                        <div class="card-article">Артикул: 00000012316</div>
                        <div class="card-option-price">
                            <div class="card-price">
                                <span class="old">₴ 4889</span>
                                <span class="new">₴ 2789</span>
                                <div class="badge action">-20%</div>
                            </div>
                            <div class="card-action-left">
                                до кінця акції залишилось
                                <div class="count">8 днів</div>
                            </div>
                        </div>
                        <div class="card-size">
                            <div class="card-size-title">Розмір</div>
                            <div class="size-group-wrap">
                                <div class="size-group">
                                    <label class="size-radio">
                                        <input type="radio" hidden name="size">
                                        <span class="size-radio-count" data-text="35"></span>
                                    </label>
                                    <label class="size-radio">
                                        <input type="radio" hidden name="size">
                                        <span class="size-radio-count" data-text="36"></span>
                                    </label>
                                    <label class="size-radio disabled">
                                        <input type="radio" hidden name="size" disabled>
                                        <span class="size-radio-count" data-text="37"></span>
                                    </label>
                                    <label class="size-radio">
                                        <input type="radio" hidden name="size">
                                        <span class="size-radio-count" data-text="38"></span>
                                    </label>
                                    <label class="size-radio">
                                        <input type="radio" hidden name="size">
                                        <span class="size-radio-count" data-text="39"></span>
                                    </label>
                                    <label class="size-radio">
                                        <input type="radio" hidden name="size">
                                        <span class="size-radio-count" data-text=40></span>
                                    </label>
                                    <div class="popover">
                                        <div class="p-title">Сповістити про поступлення розміру</div>
                                        <a href="" class="link open-popup" data-rel="size-callback">Сповістити мене</a>
                                    </div>
                                </div>
                                <a href="" class="link open-popup" data-rel="table-size">таблиця розмірів</a>
                            </div>
                        </div>
                        <div class="btn-group">
                            <a href="" class="btn btn-primary add-to-cart">Додати в кошик</a>
                            <a href="" class="btn btn-stroke open-popup" data-rel="buy-click">Купити в один клік</a>
                        </div>
                        <a href="" class="btn-mono ">
                            <img src="img/mono.png" alt="">
                            Оплата частинами від МОНО
                        </a>
                        <a href="" class="additional-info open-popup" data-rel="dop-info">
                            <img src="img/icon-info.svg" alt="">
                            <span class="link">Запитати додаткову інформацію</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="detail-card-row">
            <div class="container">
                <div class="card-variable swiper-entry">
                    <div class="block-title type-2">Доступні кольори</div>
                    <div class="swiper-button-prev button type-2"><img src="img/arrow-long-right.svg" alt=""></div>
                    <div class="swiper-button-next button type-2"><img src="img/arrow-long-right.svg" alt=""></div>
                    <div class="swiper-container" data-options='{"slidesPerView":4,"spaceBetween":32, "breakpoints":{ "767": {"slidesPerView":2,"spaceBetween":32}}}'>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="" class="variable-link">
                                    <img class="lazy" data-src="img/color-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="" class="variable-link">
                                    <img class="lazy" data-src="img/color-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="" class="variable-link">
                                    <img class="lazy" data-src="img/color-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="" class="variable-link">
                                    <img class="lazy" data-src="img/color-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="" class="variable-link">
                                    <img class="lazy" data-src="img/color-2.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="swiper-scrollbar"></div>
                    </div>
                </div>
                <div class="info">
                    <ul>
                        <li>
                            <img src="img/pay-icon.svg" alt="" class="icon">
                            <p>Оплата при отриманні</p>
                        </li>
                        <li>
                            <img src="img/icon-help-size.svg" alt="" class="icon">
                            <p>Допомога у виборі розміру</p>
                        </li>
                        <li>
                            <img src="img/icon-calendar.svg" alt="" class="icon">
                            <p>30 днів на повернення</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="card-mobile-fixed only-mob">
            <div class="cost">₴ 2789</div>
            <a href="" class="btn btn-primary open-popup" data-rel="buy-click">Купити в один клiк</a>
        </div>

        <!-- detail tabs -->
        <section>
            <? include '_detail-tabs.php';?>
        </section>

        <!-- store CARDS -->
        <section>
            <? include '_store_cards.php';?>
        </section>

        <!-- features -->
        <section>
            <? include '_features.php';?>
        </section>

        <!-- other CARDS -->
        <section class="bg-1">
            <? include '_other-products.php';?>

        </section>

    </div>


    <!-- SEO TAG -->

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

    <!--    fancybox photo gallery-->
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <script src="js/jquery.fancybox.min.js"></script>
       
    <!--    rating js-->
    <script src="js/rater-js.js"></script>
    <script src="js/rate-init.js"></script>
    
    


</body>

</html>
