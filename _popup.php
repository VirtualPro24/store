<div class="popup-content" data-rel="table-size">
    <div class="layer-close"></div>
    <div class="popup-container">
        <div class="btn-close"><span></span></div>

        <div class="modal-title block-title type-2 text-center">Таблиця розмiрiв</div>

        <div class="table-size-wrap">
            <div class="table-col">
                <div class="table-title">Жіноче взуття</div>
                <table>
                    <tr>
                        <td>розмір</td>
                        <td>см</td>
                    </tr>
                    <tr>
                        <td>34</td>
                        <td>22,5</td>
                    </tr>
                    <tr>
                        <td>35</td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td>36</td>
                        <td>23,5</td>
                    </tr>
                    <tr>
                        <td>37</td>
                        <td>24</td>
                    </tr>
                    <tr>
                        <td>38</td>
                        <td>24,5</td>
                    </tr>
                    <tr>
                        <td>39</td>
                        <td>25</td>
                    </tr>
                    <tr>
                        <td>40</td>
                        <td>25,5</td>
                    </tr>
                    <tr>
                        <td>41</td>
                        <td>26</td>
                    </tr>
                </table>
            </div>
            <div class="table-col">
                <div class="table-title">Чоловіче взуття</div>
                <table>
                    <tr>
                        <td>розмір</td>
                        <td>см</td>
                    </tr>
                    <tr>
                        <td>38</td>
                        <td>25,5</td>
                    </tr>
                    <tr>
                        <td>39</td>
                        <td>26</td>
                    </tr>
                    <tr>
                        <td>40</td>
                        <td>27</td>
                    </tr>
                    <tr>
                        <td>41</td>
                        <td>27.5</td>
                    </tr>
                    <tr>
                        <td>42</td>
                        <td>28</td>
                    </tr>
                    <tr>
                        <td>43</td>
                        <td>29</td>
                    </tr>
                    <tr>
                        <td>44</td>
                        <td>30</td>
                    </tr>
                    <tr>
                        <td>45</td>
                        <td>31</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-bottom">

            <div class="text-warning">*Певні моделі мають відхилення від розмірної сітки, уточнюйте деталі у консультантів.</div>
            <div class="block-title type-2">Как определить свой размер?</div>
            <p>Вам понадобится провести измерения с помощью сантиметровой ленты.
                Поставьте ногу на чистый лист бумаги. Отметьте крайние границы ступни и измерьте расстояние между самыми удаленными точками стопы.</p>
        </div>

    </div>
</div>

<div class="popup-content" data-rel="write-review">
    <div class="layer-close"></div>
    <div class="popup-container type-2">
        <div class="btn-close"><span></span></div>
        <div class="rate-empty-header only-mob">
            <img src="img/review-empty.jpg" alt="">
            <p class="text-center">Відгуків поки немає.
                Будьте першим!</p>
        </div>
        <div class="modal-title block-title type-2">Написати відгук</div>
        <form action="" class="review-form">
            <div class="form-row">
                <div class="input-wrap">
                    <label class="input-title">Ім'я</label>
                    <input type="text" class="input">
                </div>
                <div class="input-wrap">
                    <label class="input-title">E-mail</label>
                    <input type="email" class="input">
                </div>
                <div class="input-wrap">
                    <label class="input-title">Відгук</label>
                    <textarea name="" class="input"></textarea>
                </div>
            </div>
            <div class="review-form-bottom">
                <div class="rate-wrap">
                    <div class="rate-title">Рейтинг</div>
                    <div class="rater"></div>
                    <div class="rater-count">0</div>
                    <input type="text" hidden class="rater-input">
                </div>
                <button class="btn btn-primary open-popup" data-rel="thank-review">Залишити відгук</button>
            </div>
        </form>
    </div>
</div>


<div class="popup-content" data-rel="dop-info">
    <div class="layer-close"></div>
    <div class="popup-container type-2">
        <div class="btn-close"><span></span></div>

        <div class="modal-title block-title type-2 mb-16">Запитати додаткову інформацію</div>
        <div class="text-row">Ми раді відповісти на Ваші питання. Введіть номер телефону, на котрий ми пришлемо відповідь на Ваше питання. Постараємося відповісти максимально швидко.</div>
        <form action="" class="review-form">
            <div class="form-row">
                <div class="input-wrap">
                    <label for="name" class="input-title">Ім'я</label>
                    <input type="text" class="input" id="name">
                </div>
                <div class="input-wrap">
                    <label for="phone" class="input-title">Телефон</label>
                    <input type="tel" class="input" id="phone">
                </div>
                <div class="input-wrap">
                    <label id="message" class="input-title">Ваш запит</label>
                    <textarea name="" class="input" id="message"></textarea>
                </div>
            </div>

            <button class="btn btn-primary w100-mob">Запитати</button>
        </form>
    </div>
</div>

<div class="popup-content" data-rel="buy-click">
    <div class="layer-close"></div>
    <div class="popup-container type-2">
        <div class="btn-close"><span></span></div>

        <div class="modal-title block-title type-2 ">Швидке замовлення</div>
        <form action="" class="order-click-form">
            <div class="form-row">
                <div class="input-wrap w50">
                    <label for="name1" class="input-title">Ім'я</label>
                    <input type="text" class="input" id="name1">
                </div>
                <div class="input-wrap w50">
                    <label for="tel2" class="input-title">Телефон</label>
                    <input type="tel" class="input" id="tel2">
                </div>
            </div>

            <button class="btn btn-primary">Купити</button>
        </form>
    </div>
</div>

<div class="popup-content" data-rel="size-callback">
    <div class="layer-close"></div>
    <div class="popup-container type-2">
        <div class="btn-close"><span></span></div>

        <div class="modal-header">
            <div class="modal-title block-title type-2">Розмір </div>
            <div class="select-box select-box-line type-2">
                <select class="SelectBox" name="">
                    <option value="" selected>35</option>
                    <option value="">36</option>
                    <option value="">37</option>
                    <option value="">38</option>
                    <option value="">39</option>
                    <option value="">40</option>
                    <option value="">41</option>
                    <option value="">42</option>
                </select>
            </div>
        </div>
        <form action="" class="order-click-form">
            <div class="form-row">
                <div class="input-wrap w50">
                    <label class="input-title">Ім'я</label>
                    <input type="text" class="input">
                </div>
                <div class="input-wrap w50">
                    <label class="input-title">Телефон</label>
                    <input type="tel" class="input">
                </div>
            </div>

            <button class="btn btn-primary">Сповістити про поступлення</button>
        </form>
    </div>
</div>

<div class="popup-content" data-rel="thank-review">
    <div class="layer-close"></div>
    <div class="popup-container type-2">
        <div class="btn-close"><span></span></div>

        <div class="thank-review">
            <img src="img/review-empty.jpg" alt="">
            <div class="text">
                <p>Ми промодеруємо ваш відгук, та виставим його в найближчий час</p>
                <span>Дякуємо за відгук!</span>
            </div>
        </div>
    </div>
</div>

<div class="popup-content" data-rel="card-preview">
    <div class="layer-close"></div>
    <div class="popup-container type-card">
        <div class="btn-close"><span></span></div>

        <div class="card-preview">
            <div class="cr-sliders">
                <div class="card-thumbs">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 01">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 02">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 03">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 04">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 05">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 06">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-prev button "><img src="img/arrow-up.svg" alt=""></div>
                    <div class="swiper-button-next button "><img src="img/arrow-up.svg" alt=""></div>
                </div>
                <div class="wrap-card-image">
                    <a href="" class="add-favorite"></a>
                    <div class="swiper-container card-gallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 01">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 02">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 03">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 04">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 05">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/preview-product.jpg" alt="Slide 06">
                            </div>
                        </div>
                    </div>
                    <a href="" class="btn-video type-2">
                        <span class="icon">
                            <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7 10.6813V13.348C7 16.3251 7 17.8137 7.97101 18.3963C8.94202 18.9789 10.2555 18.2784 12.8824 16.8774L15.3824 15.5441C18.2941 13.9911 19.75 13.2146 19.75 12.0146C19.75 10.8146 18.2941 10.0382 15.3824 8.48524L12.8824 7.1519C10.2555 5.75089 8.94202 5.05039 7.97101 5.633C7 6.2156 7 7.70417 7 10.6813Z" stroke="#876B46" />
                            </svg>

                        </span>
                        посмотреть видео
                    </a>
                </div>
            </div>
            <div class="card-preview-info detail-card-info">
                <div class="detail-card-rate">
                    <div class="count">4.5</div>
                    <div class="rate-list">
                        <img src="img/star-brow.svg" alt="">
                        <img src="img/star-brow.svg" alt="">
                        <img src="img/star-brow.svg" alt="">
                        <img src="img/star-brow.svg" alt="">
                        <img class="star-empty" src="img/star-brow.svg" alt="">
                    </div>
                </div>

                <div class="in-stok">
                    <img src="img/check.svg" alt="">
                    в наявності
                </div>
                <div class="card-title h2">LE'BERDES Черевики зимові</div>
                <div class="card-article">Артикул: 00000012316</div>
                <div class="card-option-price">
                    <div class="card-price">
                        <span class="old">₴ 4889</span>
                        <span class="new">₴ 2789</span>
                        <div class="badge action">-20%</div>
                    </div>
                    <div class="card-action-left">
                        до кінця акції залишилось
                        <div class="count">8 днів</div>
                    </div>
                </div>
                <div class="card-size">
                    <div class="card-size-title">Розмір</div>
                    <div class="size-group-wrap">
                        <div class="size-group">
                            <label class="size-radio">
                                <input type="radio" hidden name="size">
                                <span class="size-radio-count" data-text="35"></span>
                            </label>
                            <label class="size-radio">
                                <input type="radio" hidden name="size">
                                <span class="size-radio-count" data-text="36"></span>
                            </label>
                            <label class="size-radio open-popup" data-rel="size-callback">
                                <input type="radio" hidden name="size" disabled>
                                <span class="size-radio-count" data-text="37"></span>
                            </label>
                            <label class="size-radio">
                                <input type="radio" hidden name="size">
                                <span class="size-radio-count" data-text="38"></span>
                            </label>
                            <label class="size-radio">
                                <input type="radio" hidden name="size">
                                <span class="size-radio-count" data-text="39"></span>
                            </label>
                            <label class="size-radio">
                                <input type="radio" hidden name="size">
                                <span class="size-radio-count" data-text=40></span>
                            </label>
                        </div>
                        <a href="" class="link open-popup" data-rel="table-size">таблиця розмірів</a>
                    </div>
                </div>
                <div class="btn-group">
                    <a href="" class="btn btn-primary add-to-cart">Додати в кошик</a>
                    <a href="" class="btn btn-stroke open-popup" data-rel="buy-click">Купити в один клік</a>
                </div>
                <div class="all-char">
                    <div class="title">Опис товару</div>
                    <div class="char-table">
                        <table>
                            <tr>
                                <td>Стать</td>
                                <td>Жіноча</td>
                            </tr>
                            <tr>
                                <td>Вид</td>
                                <td>Черевики</td>
                            </tr>
                            <tr>
                                <td>Стиль</td>
                                <td>Fashion</td>
                            </tr>
                            <tr>
                                <td>Матеріал</td>
                                <td>Натуральна шкіра</td>
                            </tr>
                            <tr>
                                <td>Матеріал підкладки</td>
                                <td>Хутро</td>
                            </tr>
                            <tr>
                                <td>Тип підошви</td>
                                <td>Низький каблук</td>
                            </tr>
                            <tr>
                                <td>Розміри</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <a href="" class="logo">
                        <img src="img/berdes.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup-content" data-rel="modal-cart">
    <div class="layer-close"></div>
    <div class="popup-container type-3">
        <div class="btn-close"><span></span></div>

        <div class="modal-cart">
            <div class="block-title type-2">В кошику (3) товара</div>

            <div class="mc-row">
                <div class="mc-table">
                    <div class="mc-tr" data-tr-product>
                        <a href="" class="mc-tr-image">
                            <img src="img/cart/cart.jpg" alt="">
                        </a>
                        <div class="mc-tr-info">
                            <div class="mc-tr-header">

                                <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                <div class="size">
                                    Розмір <span>35</span>
                                </div>
                            </div>
                            <p class="count">к-сть. 1 </p>
                            <div class="price">2789 ₴</div>

                            <div class="delete-popover" data-popover>
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mc-tr-remove"></div>
                    </div>
                    <div class="mc-tr" data-tr-product>
                        <a href="" class="mc-tr-image">
                            <img src="img/color-2.jpg" alt="">
                        </a>
                        <div class="mc-tr-info">
                            <div class="mc-tr-header">

                                <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                <div class="size">
                                    Розмір <span>35</span>
                                </div>
                            </div>
                            <p class="count">к-сть. 1 </p>
                            <div class="price">2789 ₴</div>

                            <div class="delete-popover" data-popover>
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mc-tr-remove"></div>
                    </div>
                    <div class="mc-tr" data-tr-product>
                        <a href="" class="mc-tr-image">
                            <img src="img/other-prod.jpg" alt="">
                        </a>
                        <div class="mc-tr-info">
                            <div class="mc-tr-header">

                                <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                <div class="size">
                                    Розмір <span>35</span>
                                </div>
                            </div>
                            <p class="count">к-сть. 1 </p>
                            <div class="price">2789 ₴</div>

                            <div class="delete-popover" data-popover>
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mc-tr-remove"></div>
                    </div>
                </div>
                <div class="mc-side">
                    <div class="mc-side-title">До сплати</div>
                    <div class="mc-side-total">9967 ₴</div>
                    <a href="" class="btn btn-primary">Оформити замовлення</a>
                    <div class="text-center">
                        <a href="" class="link">Перейти в кошик</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="popup-content" data-rel="empty-cart">
    <div class="layer-close"></div>
    <div class="popup-container type-3">
        <div class="btn-close"><span></span></div>
        
        <div class="modal-empty-cart">
            <div class="icon">
                <img src="img/empty-cart.svg" alt="">
            </div>
            <div class="text">
                <div class="h2">Ваш кошик пустий.</div>
                <p>Будь ласка зробіть свій вибір з каталогу</p>
            </div>
        </div>
    </div>
</div>
