<div class="detail-tabs tabs">
    <div class="container">
        <div class="tab-nav">
            <ul>
                <li data-tab="tab-1" class="active">Повні характеристики</li>
                <li data-tab="tab-2">(4) Відгуки</li>
                <li data-tab="tab-3">Відгуки</li>
                <li data-tab="tab-4">Умови доставки</li>
                <li data-tab="tab-5">Обмін та повернення</li>
                <li data-tab="tab-6">Гарантія</li>
            </ul>
        </div>

        <div class="detail-tabs-row">
            <div class="tabs-content">
                <div class="tab-item tab-1 active">
                    <div class="all-char">
                        <div class="title">Опис товару</div>
                        <div class="char-table">
                            <table>
                                <tr>
                                    <td>Стать</td>
                                    <td>Жіноча</td>
                                </tr>
                                <tr>
                                    <td>Вид</td>
                                    <td>Черевики</td>
                                </tr>
                                <tr>
                                    <td>Стиль</td>
                                    <td>Fashion</td>
                                </tr>
                                <tr>
                                    <td>Матеріал</td>
                                    <td>Натуральна шкіра</td>
                                </tr>
                                <tr>
                                    <td>Матеріал підкладки</td>
                                    <td>Хутро</td>
                                </tr>
                                <tr>
                                    <td>Тип підошви</td>
                                    <td>Низький каблук</td>
                                </tr>
                                <tr>
                                    <td>Розміри</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <a href="" class="logo">
                            <img src="img/berdes.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="tab-item review-all tab-2">
                    <div class="review-tab-header">
                        <div class="count-text">(28) Відгуків по цьому товару</div>
                        <a href="" class="btn btn-stroke type-2 btn-review open-popup" data-rel="write-review">Залишити відгук</a>
                    </div>
                    <div class="review-tab-list">
                        <div class="review-line">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                            <div class="subreview review-line">
                                <div class="review-bottom">
                                    <div class="name">FavoriteShoes</div>
                                    <div class="date">24 Січня 2021</div>
                                </div>
                                <div class="text">Дякуємо за ваш відгук!</div>
                            </div>
                        </div>
                        <div class="review-line">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                        </div>
                        <div class="review-line">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                        </div>
                        <div class="review-line hide">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                        </div>
                        <div class="review-line hide">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                        </div>
                        <div class="review-line hide">
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                                <div class="date">24 Січня 2021</div>
                            </div>
                            <div class="text">Быстая доставка, шикарная обувь, буду дальше у вас заказывать, всем рекомендую. Спасибо за работу.</div>
                        </div>                        
                    </div>
                    <a href="" class="show-all-review">Показати усі відгуки</a>
                </div>
                <div class="tab-item tab-3">
                    <div class="rate-empty">
                        <div class="rate-empty-header">
                            <img src="img/review-empty.jpg" alt="">
                            <p>Відгуків поки немає.
                                Будьте першим!</p>
                        </div>
                        <div class="block-title type-2">Написати відгук</div>
                        <form action="" class="review-form">
                            <div class="form-row"> 
                                <div class="input-wrap">
                                    <label class="input-title">Ім'я</label>
                                    <input type="text" class="input">
                                </div>
                                <div class="input-wrap">
                                    <label class="input-title">E-mail</label>
                                    <input type="email" class="input">
                                </div>  
                                <div class="input-wrap">
                                    <label class="input-title">Відгук</label>
                                    <textarea name="" class="input"></textarea>
                                </div>                                                             
                            </div>
                            <div class="review-form-bottom">
                                <div class="rate-wrap">
                                    <div class="rate-title">Рейтинг</div>
                                    <div class="rater"></div> 
                                    <div class="rater-count">0</div>
                                    <input type="text" hidden class="rater-input">
                                </div>
                                <button class="btn btn-primary">Залишити відгук</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-item tab-4">
                    <div class="detail-info">
                        <div class="detail-info-item">
                            <div class="detail-info-header">
                                <img src="img/delivery-icon.svg" alt="" class="icon">
                                <div class="block-title">Доставка</div>
                            </div>
                            <ul>
                                <li>Безкоштовна доставка при передоплаті
                                    <p>Сроки доставки 1-3 дня</p>
                                </li>
                                <li>Новою Поштою на відділення</li>
                                <li>Кур'єром "Під двері"</li>
                            </ul>
                        </div>
                        <div class="detail-info-item">
                            <div class="detail-info-header">
                                <img src="img/pay-icon.svg" alt="" class="icon">
                                <div class="block-title">Оплата</div>
                            </div>
                            <ul>
                                <li>Готівкою при отриманні на відділенні
                                    <p>Вартість послуги: 2%+20грн</p>
                                </li>
                                <li>Оплата через Приват24</li>
                                <li>Карткою Visa/MasterCard</li>
                                <li>Готівкою в наших магазинах</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-item tab-5">
                    <div class="detail-info">
                        <div class="detail-info-item">
                            <div class="detail-info-header">
                                <img src="img/icon-change.svg" alt="" class="icon">
                                <div class="block-title">Обмін</div>
                            </div>
                            <ul>
                                <li>Гарантований обмін товару 
                                    <p>(протягом 14 днів з моменту отримання)</p>
                                </li>
                                <li>Обмін проводиться безкоштовно</li>
                                <li>Процедура займе мінімум Вашого часу
                                    <p>(від 2-х до 5-ти днів)</p>    
                                </li> 
                            </ul>
                        </div>
                        <div class="detail-info-item">
                            <div class="detail-info-header">
                                <img src="img/icon-return.svg" alt="" class="icon">
                                <div class="block-title">Повернення</div>
                            </div>
                            <ul>
                                <li>Гарантоване повернення коштів
                                    <p>протягом 14 днів з моменту отримання</p>
                                </li>
                                <li>Повернення товару оплачує покупець</li>
                                <li>Поверення коштів на картку протягом 5 робочих днів
                                    <p>(з моменту поступлення товару на наш склад)</p>
                                </li> 
                            </ul>
                        </div>
                    </div>                    
                </div>
                <div class="tab-item tab-6">
                    <div class="garanty">
                        <ul>
                            <li>Наше взуття виготовлено виключно з натуральних матеріалів ззовні і всередині
                                <p>У нас немає взуття із замінника</p>
                            </li>
                            <li>Наше взуття виготовлено виключно з натуральних матеріалів ззовні і всередині
                                <p>У нас немає взуття із замінника</p>
                            </li>
                            <li>Всі фото, представлені на сайті, не піддавались фотокорекції та на 100% відповідають реальному вигляду товару
                                <p>Наші консультанти надішлють реальні фото на Viber або e-mail за Вашим бажанням</p>
                            </li>
                            <li>
                                Ми впевнені в якості наших виробів та надаємо гарантійне обслуговування на весь асотримент
                                <p>У разі гарантійного випадку ми повертаємо кошти, обмінюємо товар або ж оплачуємо ремонт</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="question-social only-mob">
                <div class="block-title type-2">Остались вопросы? Напишите нам!</div>
                <div class="btn-group">
                    <a href="" class="btn btn-stroke type-2 btn-viber">Viber</a>
                    <a href="" class="btn btn-stroke type-2 btn-telegram">Telegram</a>
                </div>
            </div>
            <div class="with-product">
                <div class="block-title type-2">С цим товаром купують</div>
                <div class="list">
                    <div class="store-card">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/store-cards/product-with.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">Рюкзак Banlegtaga </div>
                            <div class="price">
                                <span class="new">3249 ₴</span>
                            </div>
                        </div>
                    </div>
                    <div class="store-card">
                        <a href="" class="link-card"></a>
                        <div class="image"><img class="lazy" data-src="img/store-cards/product-with-2.jpg" alt=""></div>
                        <div class="sc-content">
                            <div class="sc-title">Рюкзак Banlegtaga </div>
                            <div class="price">
                                <span class="new">3249 ₴</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer-100"></div>
</div>
