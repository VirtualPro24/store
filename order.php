<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Мій профіль</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page settings-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Настройки</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="cabinet-nav">
                    <ul>
                        <li><a href="cabinet.php">Мій профіль</a></li>
                        <li><a href="order.php" class="active">Замовлення</a></li>
                        <li><a href="">Обране</a></li>
                    </ul>
                </div>

                <div class="orders-wrapper">
                    <div class="orders-header order--tr">
                        <div class="order--td td-1">№ Замовлення</div>
                        <div class="order--td td-2">Товари</div>
                        <div class="order--td td-3">К-сть.</div>
                        <div class="order--td td-4">Дата</div>
                        <div class="order--td td-5">Статус</div>
                        <div class="order--td td-6">ТТН</div>
                    </div>
                    <div class="orders-body">
                        <div class="order-wrap">
                            <div class="order--tr">
                                <div class="order--td td-1" data-td="№">2124</div>
                                <div class="order--td td-2">
                                    <div class="order-prod">
                                        <div class="image"><img src="img/preview-product.jpg" alt=""></div>
                                        <div class="image"><img src="img/preview-product.jpg" alt=""></div>
                                    </div>
                                </div>
                                <div class="order--td td-3" data-td="К-сть.">1</div>
                                <div class="order--td td-4" data-td="Дата">8 Лютого 2021</div>
                                <div class="order--td td-5" data-td="Статус">
                                    <div class="status in-progress">в обробцi</div>
                                </div>
                                <div class="order--td td-6">-</div>
                                <div class="arrow-drop"></div>
                            </div>
                            <div class="order--tr tr-hide">
                                <div class="order--td td-1"></div>
                                <div class="order--td td-2">
                                    <div class="order-prod">
                                        <div class="image"><img src="img/preview-product.jpg" alt=""></div>
                                        <div class="order-prod-text">
                                            <a href="" class="order-prod-title">LE'BERDES Черевики зимові</a>
                                            <div class="price">2789 ₴</div>
                                            <div class="article">АРТИКУЛ: 00000012345</div>
                                            <div class="size">Розмір: 35</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order--td td-3">1</div>
                            </div>
                            <div class="order--tr tr-hide">
                                <div class="order--td td-1"></div>
                                <div class="order--td td-2">
                                    <div class="order-prod">
                                        <div class="image"><img src="img/preview-product.jpg" alt=""></div>
                                        <div class="order-prod-text">
                                            <a href="" class="order-prod-title">LE'BERDES Черевики зимові</a>
                                            <div class="price">2789 ₴</div>
                                            <div class="article">АРТИКУЛ: 00000012345</div>
                                            <div class="size">Розмір: 35</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order--td td-3">1</div>
                            </div>
                            <div class="order-detail">
                                <div class="title text-15 fw-500">Детальна інформація:</div>

                                <div class="od-item">
                                    <div class="small-13 fw-500">купон на знижку</div>
                                    <div class="od-coupon text-15">-20FAVORITSHOESXX5as</div>
                                    <ul class="od-discount">
                                        <li>
                                            Сумма знижки:
                                            <div class="text-15 fw-500">-1000 ₴</div>
                                        </li>
                                        <li>
                                            Сумма товарів:
                                            <div class="text-15 fw-500">5578 ₴</div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Доставка</div>
                                    <ul class="od-delivery">
                                        <li>
                                            <b>Місто:</b>
                                            Хмельницький
                                        </li>
                                        <li>
                                            <b>Курьером</b> вул.Майборського 15, кв.56
                                        </li>
                                    </ul>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Дата замовлення:</div>
                                    <div class="od-time">
                                        <span>8 Лютого 2021</span>
                                        12:20
                                    </div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Оплата</div>
                                    <div class="od-pay">
                                        <img src="img/pay/icon-wallet.svg" alt="">
                                        Готівкою при отриманні
                                    </div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Статус</div>
                                    <div class="status in-progress">в обробцi</div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">ТТН</div>
                                    <span class="fw-500 small-13">_</span>
                                </div>
                            </div>
                            <div class="arrow-drop mobile-arrow"></div>
                        </div>
                        <div class="order-wrap">
                            <div class="order--tr">
                                <div class="order--td td-1" data-td="№">2124</div>
                                <div class="order--td td-2">
                                    <div class="order-prod">
                                        <div class="image"><img src="img/preview-product.jpg" alt=""></div>
                                        <div class="order-prod-text">
                                            <a href="" class="order-prod-title">LE'BERDES Черевики зимові</a>
                                            <div class="price">2789 ₴</div>
                                            <div class="old-price">3789 ₴</div>
                                            <div class="only-mob count">к-сть. 1</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order--td td-3" data-td="К-сть.">1</div>
                                <div class="order--td td-4" data-td="Дата">8 Лютого 2021</div>
                                <div class="order--td td-5" data-td="Статус">
                                    <div class="status done">вiдправленно</div>
                                </div>
                                <div class="order--td td-6"><span>2821032934532</span></div>
                            </div>

                            <div class="order-detail">
                                <div class="title text-15 fw-500">Детальна інформація:</div>

                                <div class="od-item">
                                    <div class="small-13 fw-500">купон на знижку</div>
                                    <div class="od-coupon text-15">-20FAVORITSHOESXX5as</div>
                                    <ul class="od-discount">
                                        <li>
                                            Сумма знижки:
                                            <div class="text-15 fw-500">-1000 ₴</div>
                                        </li>
                                        <li>
                                            Сумма товарів:
                                            <div class="text-15 fw-500">5578 ₴</div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Доставка</div>
                                    <ul class="od-delivery">
                                        <li>
                                            <b>Місто:</b>
                                            Хмельницький
                                        </li>
                                        <li>
                                            <b>Курьером</b> вул.Майборського 15, кв.56
                                        </li>
                                    </ul>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Дата замовлення:</div>
                                    <div class="od-time">
                                        <span>8 Лютого 2021</span>
                                        12:20
                                    </div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Оплата</div>
                                    <div class="od-pay">
                                        <img src="img/pay/icon-wallet.svg" alt="">
                                        Готівкою при отриманні
                                    </div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">Статус</div>
                                    <div class="status done">вiдправленно</div>
                                </div>
                                <div class="od-item">
                                    <div class="od-item-title text-15 fw-500">ТТН</div>
                                    <span class="fw-500 small-13">2821032934532</span>
                                </div>
                            </div>
                            <div class="arrow-drop mobile-arrow"></div>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
