<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Оплата та доставка</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page public-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Оплата та доставка</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>



                <div class="public-row">
                    <div class="public-sidebar" data-side>
                        <div class="public-item">
                            <div class="pi-header">Основні терміни </div>
                            <div class="pi-content">
                                <ul>
                                    <li><a href="">Покупець </a></li>
                                    <li><a href="">Продавець </a></li>
                                    <li><a href="">Інтернет-магазин</a></li>
                                    <li><a href="">Товар - взуття</a></li>
                                    <li><a href="">Замовлення</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="public-item">
                            <div class="pi-header">Загальні положення</div>
                            <div class="pi-content">
                                <ul>
                                    <li><a href="">Продавець здійснює продаж Товарів  </a></li>
                                    <li><a href="">Замовляючи Товари </a></li>
                                    <li><a href="">Ці Умови продажу товарів</a></li>
                                    <li><a href="">Ці умови можуть бути змінені</a></li>
                                    <li><a href="">Договір роздрібної купівлі-продажу</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="public-item">
                            <div class="pi-header">Товар та порядок здійснення покупки </div>
                            <div class="pi-content">
                                <ul>
                                    <li><a href="">Продавець не забезпечує наявність на своєму складі </a></li>
                                    <li><a href="">У разі відсутності замовлених Покупцем </a></li>
                                    <li><a href="">Покупець несе повну відповідальність</a></li>
                                    <li><a href="">У разі скасування повністю або частково передплаченого</a></li>
                                    <li><a href="">Замовлення Покупця оформлюється відповідно</a></li>
                                </ul>
                            </div>
                        </div>                                                
                    </div>
                    <div class="public-content simple-text" data-content>
                        <h2>Основні терміни </h2>
                        <p id="id-1-1"><b>1.1.Покупець</b> - будь-яка дієздатна фізична, юридична особа, фізична особа-підприємець, згідно чинного українського законодавства, що відвідали Веб-сайт favoriteshoes.com.ua і мають намір придбати той чи інший Товар. </p>
                        <p id="id-1-2"><b>1.2.Покупець</b> - Продавець — юридична особа або фізична особа-підприємець, товар яких розміщений в Інтернет-магазині. Діяльність магазину здійснюється від</p>
                        <p ><b>ФОП</b> Сушко Х.Р.
                           <b> ІПН</b> 3439105441
                            <b>IBAN</b>: UA573052990000026003011021380
                            <b>АТ КБ</b> "ПРИВАТБАНК"</p>
                        <p id="id-1-3"><b>1.3 Покупець</b> - Інтернет-магазин - Веб-сайт, розташований в мережі інтернет за адресою favoriteshoes.com.ua, де представлені Товари, пропоновані Продавцем для придбання, а також умови оплати і доставки Товарів Покупцям. Веб-сайт — favoriteshoes.com.ua.</p>
                        <p id="id-1-4"><b>1.4 Товар</b> - взуття, сумки, аксесуари та інші товари, представлені до продажу на Веб-сайті Продавця. </p>
                        <p id="id-1-5"><b>1.5 Замовлення</b> - належним чином оформлений запит Покупця на придбання і доставку за вказаною Покупцем адресою Товарів, обраних на Веб-сайті. </p>
                        
                        <h2>Загальні положення</h2>
                        <p id="id-2-1"><b>2.1.</b> Продавець здійснює продаж Товарів через Інтернет-магазин за адресою favoriteshoes.com.ua. </p>
                        <p id="id-2-2"><b>2.2.</b> Замовляючи Товари через Інтернет-магазин, Покупець погоджується з умовами продажу Товарів, викладеними нижче (далі - Умови продажу товарів). </p>
                        <p id="id-2-3"><b>2.3</b> Ці Умови продажу товарів, а також інформація про товар, представлена на Веб-сайті, є публічною офертою, відповідно до ст. 633 Цивільного кодексу України. </p>
                        <p id="id-2-4"><b>2.4</b> Ці умови можуть бути змінені Продавцем в односторонньому порядку без повідомлення Покупця. Нова редакція Умов набуває чинності з моменту її опублікування на Веб-сайті, якщо не буде передбачено інше. </p>
                        <p id="id-1-5"><b>2.3</b> Ці Умови продажу товарів, а також інформація про товар, представлена на Веб-сайті, є публічною офертою, відповідно до ст. 633 Цивільного кодексу України. </p>
 
                        
                        <h2>Товар та порядок здійснення покупки </h2>
                        <p><b>2.1.</b> Продавець здійснює продаж Товарів через Інтернет-магазин за адресою favoriteshoes.com.ua. </p>
                        <p><b>2.2.</b> Замовляючи Товари через Інтернет-магазин, Покупець погоджується з умовами продажу Товарів, викладеними нижче (далі - Умови продажу товарів). </p>
                        <p><b>2.3</b> Ці Умови продажу товарів, а також інформація про товар, представлена на Веб-сайті, є публічною офертою, відповідно до ст. 633 Цивільного кодексу України. </p>
                        <p><b>2.4</b> Ці умови можуть бути змінені Продавцем в односторонньому порядку без повідомлення Покупця. Нова редакція Умов набуває чинності з моменту її опублікування на Веб-сайті, якщо не буде передбачено інше. </p>
                        <p><b>2.3</b> Ці Умови продажу товарів, а також інформація про товар, представлена на Веб-сайті, є публічною офертою, відповідно до ст. 633 Цивільного кодексу України. </p>
                        <p><b>2.4</b> Ці умови можуть бути змінені Продавцем в односторонньому порядку без повідомлення Покупця. Нова редакція Умов набуває чинності з моменту її опублікування на Веб-сайті, якщо не буде передбачено інше. </p>                                             
                                              
                    </div>
                </div>

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
