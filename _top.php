<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<meta name='robots' content='noindex,nofollow' />
<link rel="shortcut icon" href="img/favicon.ico" /> 
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">

<!-- PRELOAD CSS --> 
<link href="css/main.css" rel="stylesheet" type="text/css"/>

<!-- FONTS -->
<style>

    @font-face {
        font-family: 'Gotham Pro';
        src: url('fonts/GothamPro-Medium.eot');
        src: local('Gotham Pro Medium'), local('GothamPro-Medium'),
            url('fonts/GothamPro-Medium.eot?#iefix') format('embedded-opentype'),
            url('fonts/GothamPro-Medium.woff') format('woff'),
            url('fonts/GothamPro-Medium.ttf') format('truetype');
        font-weight: 500;
        font-style: normal;
    }
    @font-face {
        font-family: 'Gotham Pro';
        src: url('fonts/GothamPro-Bold.eot');
        src: local('Gotham Pro Bold'), local('GothamPro-Bold'),
            url('fonts/GothamPro-Bold.eot?#iefix') format('embedded-opentype'),
            url('fonts/GothamPro-Bold.woff') format('woff'),
            url('fonts/GothamPro-Bold.ttf') format('truetype');
        font-weight: bold;
        font-style: normal;
    }

    @font-face {
        font-family: 'Gotham Pro';
        src: url('fonts/GothamPro-Light.eot');
        src: local('Gotham Pro Light'), local('GothamPro-Light'),
            url('fonts/GothamPro-Light.eot?#iefix') format('embedded-opentype'),
            url('fonts/GothamPro-Light.woff') format('woff'),
            url('fonts/GothamPro-Light.ttf') format('truetype');
        font-weight: 300;
        font-style: normal;
    }
 
    @font-face {
        font-family: 'Gotham Pro';
        src: url('fonts/GothamPro.eot');
        src: local('Gotham Pro'), local('GothamPro'),
            url('fonts/GothamPro.eot?#iefix') format('embedded-opentype'),
            url('fonts/GothamPro.woff') format('woff'),
            url('fonts/GothamPro.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
    }
    
*{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;-webkit-text-size-adjust:none;box-sizing:border-box}html:not(.touch-screen)::-webkit-scrollbar,html:not(.touch-screen) *::-webkit-scrollbar{width:3px;height:6px}html:not(.touch-screen)::-webkit-scrollbar-track,html:not(.touch-screen) *::-webkit-scrollbar-track{background:#e1e1e1}html:not(.touch-screen)::-webkit-scrollbar-thumb,html:not(.touch-screen) *::-webkit-scrollbar-thumb{background:#A28054}:focus,:active,:visited{outline:none}html{height:100%;overflow-x:hidden}body{height:100%;overflow:hidden}body{font-family:'Gotham Pro';color:#000}a{text-decoration:none;outline:none;color:inherit}a:link,a:visited{text-decoration:none;outline:none}html{height:auto;overflow-y:scroll}body{height:auto;overflow:visible}    
</style>



<!--ONLY ORDER PAGES-->
<link href="css/order-main.css" rel="stylesheet" type="text/css"/>

<!--ONLY login, signup PAGES-->
<link href="css/login.css" rel="stylesheet" type="text/css"/>

<!--ONLY inner PAGES-->
<link href="css/inner-page.css" rel="stylesheet" type="text/css"/>

<!--ONLY cabinet PAGES-->
<link href="css/cabinet.css" rel="stylesheet" type="text/css"/>