<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Встановити новий пароль</title>
</head>

<body class="home-page">

    <div id="content-block" class="bg-1">

        <div class="sign page">
            <div class="sign-header">
                <a href="categories.php" class="link-back">Назад в магазин</a>
                <a href="" class="logo only-mob">
                    <img src="img/logo.svg" alt="">
                </a>
            </div>
            <div class="sign-page-content">
                <div class="sign-row">
                    <div class="sign-image sign-col">
                        <img src="img/sign/sign-img.jpg" alt="">
                    </div>
                    <div class="sign-col sign-content recover bg-2">
                        <a href="" class="logo hidden-xs">
                            <img src="img/logo.svg" alt="">
                        </a>
                        <div class="title block-title type-2">Встановити новий пароль</div>
                        <div class="sign-form">
                            <form action="">
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/sign/icon-lock.svg" alt="" class="icon">
                                    <label for="t1" class="input-title">Новий пароль</label>
                                    <input type="password" id="t1" class="input"> 
                                    <span class="icon-change-view" data-show-password></span>
                                </div>                               
                                <div class="input-wrap type-2 with-icon wrap-white">
                                    <img src="img/sign/icon-lock.svg" alt="" class="icon">
                                    <label for="t2" class="input-title">Повторити пароль</label>
                                    <input type="password" id="t2" class="input">
                                    <span class="icon-change-view" data-show-password></span>
                                </div> 
                                <button class="btn btn-primary disabled" disabled>Зберегти і перейти в магазин</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>
</html>