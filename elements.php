<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Elements</title>
</head>

<body class="home-page">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header bg-1"></div>


        <div class="container" style="margin: 40px auto">
            <div class="h2" style="margin-bottom:25px"> Modals</div>
            <a href="" class="btn btn-primary open-popup" style="margin: 0 15px 10px;" data-rel="modal-cart">В кошику (3) товара</a>
            <a href="" class="btn btn-primary open-popup" style="margin: 0 15px 10px;" data-rel="empty-cart">Ваш кошик пустий</a>

        </div>
    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
