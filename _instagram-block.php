<div class="insta">
    <div class="container swiper-entry">
        <div class="insta-header">
            <div class="insta-info">
                <div class="icon">
                    <img src="img/insta.svg" alt="">
                </div>
                <div class="text">
                    <a href="" class="link-site">favoriteshoes.ua</a>
                    <ul>
                        <li>
                            <div class="number">1 654</div>
                            дописів
                        </li>
                        <li>
                            <div class="number">21,6тис.</div>
                            читачів
                        </li>
                        <li>
                            Стежить:
                            <div class="number">386</div>
                        </li>
                    </ul>
                </div>
            </div>
            <a href="" class="btn-insta">Перейти в інстаграм</a>
        </div>

        <div class="insta-slider swiper-container" data-options='{"slidesPerView":4,"spaceBetween":0, "breakpoints":{"991": {"slidesPerView":3, "spaceBetween":0}, "767": {"slidesPerView":2,"spaceBetween":0}}}'>
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-1.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-2.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-3.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-1.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-2.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-3.jpg" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img class="lazy"  data-src="img/insta/image-1.jpg" alt="">
                    </a>
                </div>                
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
</div>
