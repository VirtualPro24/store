<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Оформлення замовлення</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="header">
            <? include '_header-order.php';?>
        </header>


        <div class="checkout">
            <div class="container">
                <div class="page-header">
                    <a href="cart.php" class="link-back">Назад в корзину</a>
                    <div class="h2 text-center fw-500">Оформлення замовлення</div>
                </div>

                <div class="checkout-content">
                    <div class="checkout-col" data-sticky-scroll>
                        <div class="personal-data checkout-item">
                            <div class="checkout-item-header">
                                <img src="img/user.svg" alt="">
                                Персональні данні
                            </div>
                            <div class="checkout-login">
                                Вже маєте аккаунт?
                                <a href="" class="btn btn-stroke">Увійти</a>
                            </div>
                            <form action="">
                                <div class="input-wrap type-2 with-icon">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t1" class="input-title">Прізвище</label>
                                    <input type="text" id="t1" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t2" class="input-title">Ім’я</label>
                                    <input type="text" id="t2" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon">
                                    <img src="img/icon-email.svg" alt="" class="icon">
                                    <label for="t3" class="input-title">E-mail</label>
                                    <input type="email" id="t3" class="input">
                                </div>
                                <div class="input-wrap type-2 with-icon">
                                    <img src="img/icon-phone.svg" alt="" class="icon">
                                    <label for="t4" class="input-title">Телефон</label>
                                    <input type="tel" id="t4" class="input">
                                </div>
                            </form>
                            <div class="required">Усі поля є обовязковими</div>
                        </div>

                        <div class="delivery-method checkout-item">
                            <div class="checkout-item-header">
                                <img src="img/icon-delivery.svg" alt="">
                                Доставка
                            </div>
                            <div class="delivery-type-nav">
                                <label class="delivery-radio">
                                    <input type="radio" name="delivery-method" checked hidden>
                                    <span class="delivery-item-check btn btn-stroke">
                                        <img src="img/cart/Nova_Poshta.svg" alt="">
                                    </span>
                                </label>
                                <label class="delivery-radio">
                                    <input type="radio" name="delivery-method" hidden>
                                    <span class="delivery-item-check btn btn-stroke btn-pickup">
                                        <img src="img/empty-cart.svg" class="w24" alt="">
                                        <span>Самовивіз</span>
                                    </span>
                                </label>
                            </div>
                            
                            <div class="top-city">
                                <ul>
                                    <li data-city="Київ">Київ</li>
                                    <li data-city="Львів">Львів</li>
                                    <li data-city="Харків">Харків</li>
                                    <li data-city="Дніпро">Дніпро</li>
                                    <li data-city="Одеса">Одеса</li>
                                </ul>
                            </div>
                            
                            <div class="delyvery-type" data-delivery-item="type-1">
                                <div class="select-box type-block">
                                    <img src="img/icon-marker.svg" alt="" class="select-box-icon">
                                    <div class="select-box-title">Місто</div>
                                    <select class="SelectBox search" data-search="Виберіть місто" name="">
                                        <option value="" selected disabled>Виберіть місто</option>
                                        <option value="">Львів</option>
                                        <option value="">Київ</option>
                                        <option value="">Одеса</option>
                                    </select>
                                </div>
                                <div class="radio-group">
                                    <label class="item-radio">
                                        <input type="radio" checked name="delivery" hidden>
                                        <span class="radio-check"></span>
                                        <span class="radio-text">Кур'єром</span>
                                    </label>
                                    <label class="item-radio">
                                        <input type="radio" name="delivery" hidden>
                                        <span class="radio-check"></span>
                                        <span class="radio-text">В отделение </span>
                                    </label>
                                </div>
                                <div class="form-row p16">
                                    <div class="input-wrap type-2">
                                        <label class="input-title" for="street">Вкажіть вулицю</label>
                                        <input type="text" class="input" id="street">
                                    </div>
                                    <div class="input-wrap type-2 w50">
                                        <label class="input-title" for="house">Дом №</label>
                                        <input type="text" class="input" id="house">
                                    </div>
                                    <div class="input-wrap type-2 w50">
                                        <label class="input-title" for="flat">Квартира</label>
                                        <input type="text" class="input" id="flat">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pay-method checkout-item">
                            <div class="checkout-item-header">
                                <img src="img/cart/icon-wallet.svg" alt="">
                                Оплата
                            </div>
                            <div class="pay-group">
                                <label class="pay-item-radio item-cash">
                                    <input type="radio" name="pay-method" checked hidden>
                                    <span class="pay-item-check">
                                        <span class="icon"><img src="img/pay-icon.svg" alt=""></span>
                                        <span class="pay-item-title">Готівкою при отриманні</span>
                                    </span>
                                </label>
                                <label class="pay-item-radio">
                                    <input type="radio" name="pay-method" hidden>
                                    <span class="pay-item-check">
                                        <span class="icon"><img src="img/cart/pay-card.svg" alt=""></span>
                                        <span class="pay-item-title">Оплата карткою VISA/MasterCard
                                            <span class="description">Вас переадресує на сторінку оплати
                                                після оформлення замовлення</span>
                                        </span>
                                    </span>
                                </label>
                                <label class="pay-item-radio">
                                    <input type="radio" name="pay-method" hidden>
                                    <span class="pay-item-check">
                                        <span class="icon">
                                            <img src="img/cart/pay-privat.svg" alt="">
                                        </span>
                                        <span class="pay-item-title">На картку Приватбанк
                                            <span class="description">Реквізити надійдуть Вам в СМС</span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>

                        <div class="add-comment">
                            <a href="" class="btn btn-stroke">Додати коментар до замовлення</a>
                            <div class="add-comment-block">

                                <div class="input-wrap type-2">
                                    <!--                                    <label for="comment1" class="input-title">Ваш коментар</label>-->
                                    <textarea name="" maxlength="2000" class="input type-2" id="comment1" placeholder="Ваш коментар"></textarea>
                                </div>
                                <p><span class="count-area">0</span>/<span class="max-count">2000</span></p>
                            </div>
                        </div>
                    </div>

                    <div class="checkout-col">
                        <div class="checkout-info" data-sticky>
                            <div class="block-title type-2">Ваші товари (3)</div>
                            <div class="mc-table">
                                <div class="mc-tr" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/cart/cart.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                            <div class="size">
                                                Розмір <span>35</span>
                                            </div>
                                        </div>
                                        <p class="count">к-сть. 1 </p>
                                        <div class="price">2789 ₴</div>
                                    </div> 
                                </div>
                                <div class="mc-tr" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/color-2.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>
                                            <div class="size">
                                                Розмір <span>35</span>
                                            </div>
                                        </div>
                                        <p class="count">к-сть. 1 </p>
                                        <div class="price">2789 ₴</div> 
                                    </div> 
                                </div>
                                <div class="mc-tr hide" data-tr-product>
                                    <a href="" class="mc-tr-image">
                                        <img src="img/other-prod.jpg" alt="">
                                    </a>
                                    <div class="mc-tr-info">
                                        <div class="mc-tr-header">

                                            <a href="" class="mc-tr-title">LE'BERDES Черевики зимові</a>

                                        </div>
                                        <p class="count">к-сть. 1 </p>
                                        <div class="price">2789 ₴</div> 
                                    </div> 
                                </div>
                            </div>
                            <a href="" class="link open-order-product" data-text="показати всі товари" data-text-active="приховати товари">показати всі товари</a>
                            <div class="promo-block">
                                <a href="" class="btn btn-stroke type-2 btn-promo" data-open-promo>В мене є промокод</a>

                                <div class="check-promo" data-promo-item>
                                    <div class="check-promo-header">
                                        <p>Введіть промокод</p>
                                        <div class="check-promo-close link" data-promo-close>Закрити</div>
                                    </div>
                                    <div class="promo-input-wrap">
                                        <img src="img/cart/icon-ticket.svg" alt="" class="icon">
                                        <input type="text" placeholder="Введіть промокод">
                                        <div class="apply-wrap">
                                            <img src="img/icon-check.svg" alt="" class="apply-check">
                                            <a href="" class="check-promo-apply link">Підтвердити</a>
                                        </div>
                                    </div>
                                    <div class="input-error">Цей промокод вже не дійсний</div>
                                </div>
                            </div>
                            <div class="mc-side">
                                <div class="discount-block">
                                    <div class="item">
                                        <div class="mc-side-title">Сумма замовлення</div>
                                        <div class="price">11 247 ₴</div>
                                    </div>
                                    <div class="item">
                                        <div class="mc-side-title">Сумма знижки</div>
                                        <div class="price discount-count">— 1 280 ₴</div>
                                    </div>
                                </div>
                                <div class="mc-side-title">До сплати</div>
                                <div class="mc-side-total">9967 ₴</div>
                                <a href="" class="btn btn-primary">Оформити замовлення</a>
                            </div>
                            <label class="checkbox-item">
                                <input type="checkbox" hidden>
                                <span class="checkbox-check">
                                    Не потрiбно телефонувати для пiдтвердження замовленння
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <? include '_footer-checkout.php';?>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>
    
    

    <? include '_bottom.php';?>

</body>

</html>
