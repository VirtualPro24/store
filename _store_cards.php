<div class="srore-cards">
    <div class="spacer-60"></div>
    <div class="container swiper-entry">
        <div class="block-title">Нещодавно Ви переглядали</div>
        <div class="store-cards-slider swiper-container" data-options='{"slidesPerView":6, "loop":false,"spaceBetween":35, "breakpoints":{"991": {"slidesPerView":4,"spaceBetween":16}, "680": {"slidesPerView":1,  "loop":true, "spaceBetween":16}}}'>

            <div class="swiper-wrapper">
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-2.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-3.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div> 
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-2.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-3.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div> 
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-3.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
                <div class="store-card swiper-slide">
                    <a href="" class="link-card"></a>
                    <div class="image"><img class="lazy" data-src="img/store-cards/product-card-2.jpg" alt=""></div>
                    <div class="sc-content">
                        <div class="sc-title">LE'BERDES сумка</div>
                        <div class="price">
                            <span class="new">3249 ₴</span>
                            <span class="old">4249 ₴</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
    <div class="spacer-60"></div>
</div>
