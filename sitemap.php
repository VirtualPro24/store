<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Мапа сайту</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block" class="bg-2">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="simple-page bg-1-mob">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Мапа сайту</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="title-page h2">Мапа сайту</div>

                <div class="sitemap">
                    <div class="sitemap-item">
                        <div class="sitemap-item-title">ДОПОМОГА</div>
                        <ul>
                            <li><a href="">Допомога</a></li>
                            <li><a href="">Доставка</a></li>
                            <li><a href="">Адреса магазинів</a></li>
                            <li><a href="">Оплата</a></li>
                            <li><a href="">Гарантія</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Відгукі обмін</a></li>
                            <li><a href="">Новини та акції</a></li>
                        </ul>
                    </div>
                    <div class="sitemap-item type-2">
                        <div class="sitemap-item-title">Жіноче взуття</div>
                        <ul>
                            <li><a href="">Туфлі</a></li>
                            <li><a href="">Черевики</a></li>
                            <li><a href="">Чоботи</a></li>
                            <li><a href="">Ботфорти</a></li>
                            <li><a href="">Кросівки та кеди</a></li>
                            <li><a href="">Ботільйони</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                            <li><a href="">Еспадрильї</a></li>
                            <li><a href="">Уги</a></li>
                            <li><a href="">Балетки</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                        </ul>
                    </div>  
                    <div class="sitemap-item">
                        <div class="sitemap-item-title">чоловіче взуття</div>
                        <ul>
                            <li><a href="">Допомога</a></li>
                            <li><a href="">Доставка</a></li>
                            <li><a href="">Адреса магазинів</a></li>
                            <li><a href="">Оплата</a></li>
                            <li><a href="">Гарантія</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Відгукі обмін</a></li>
                            <li><a href="">Новини та акції</a></li>
                        </ul>
                    </div>
                    <div class="sitemap-item type-2">
                        <div class="sitemap-item-title">Акції</div>
                        <ul>
                            <li><a href="">Туфлі</a></li>
                            <li><a href="">Черевики</a></li>
                            <li><a href="">Чоботи</a></li>
                            <li><a href="">Ботфорти</a></li>
                            <li><a href="">Кросівки та кеди</a></li>
                            <li><a href="">Ботільйони</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                            <li><a href="">Еспадрильї</a></li>
                            <li><a href="">Уги</a></li>
                            <li><a href="">Балетки</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                        </ul>
                    </div>   
                    <div class="sitemap-item type-2">
                        <div class="sitemap-item-title">Жіноче взуття</div>
                        <ul>
                            <li><a href="">Туфлі</a></li>
                            <li><a href="">Черевики</a></li>
                            <li><a href="">Чоботи</a></li>
                            <li><a href="">Ботфорти</a></li>
                            <li><a href="">Кросівки та кеди</a></li>
                            <li><a href="">Ботільйони</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                            <li><a href="">Еспадрильї</a></li>
                            <li><a href="">Уги</a></li>
                            <li><a href="">Балетки</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                        </ul>
                    </div>  
                    <div class="sitemap-item">
                        <div class="sitemap-item-title">чоловіче взуття</div>
                        <ul>
                            <li><a href="">Допомога</a></li>
                            <li><a href="">Доставка</a></li>
                            <li><a href="">Адреса магазинів</a></li>
                            <li><a href="">Оплата</a></li>
                            <li><a href="">Гарантія</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Публічна оферта</a></li>
                            <li><a href="">Відгукі обмін</a></li>
                            <li><a href="">Новини та акції</a></li>
                        </ul>
                    </div>
                    <div class="sitemap-item type-2">
                        <div class="sitemap-item-title">Акції</div>
                        <ul>
                            <li><a href="">Туфлі</a></li>
                            <li><a href="">Черевики</a></li>
                            <li><a href="">Чоботи</a></li>
                            <li><a href="">Ботфорти</a></li>
                            <li><a href="">Кросівки та кеди</a></li>
                            <li><a href="">Ботільйони</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                            <li><a href="">Еспадрильї</a></li>
                            <li><a href="">Уги</a></li>
                            <li><a href="">Балетки</a></li>
                            <li><a href="">Сабо</a></li>
                            <li><a href="">Сандалі</a></li>
                            <li><a href="">Шльопанці</a></li>
                        </ul>
                    </div>                                                             
                </div>
            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
