<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Categories</title>
</head>

<body class="home-page">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header bg-1"></div>

        <div class="categories-top bg-1">
            <div class="container">
                <div class="breadcrumbs hidden-xs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Жіноче взуття</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>
                <a href="" class="mobile-back only-mob">Жіноче взуття</a> 
                <div class="page-title h2 hidden-xs">Пошук “<b>кроссівки жіночі</b>”</div>
 
            </div>
        </div>

        <div class="products-section">
            <div class="container">
                <div class="products-header type-search">
                   <div class="products-count">336 Товарів</div>
                    <div class="sort">
                        <div class="sort-text">Сортувати:</div>
                        <div class="select-box select-box-line">
                            <select class="SelectBox" name="">
                                <option value="" selected>За цiною</option>
                                <option value="">За зростанням ціни</option>
                            </select>
                        </div>
                    </div>
 

                </div>

                <div class="product-list">
                    <div class="product" itemscope itemtype="https://schema.org/Product">
                        <div class="product-main">
                            <a href="" class="add-favorite active"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" itemprop="image" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title" itemprop="name">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new" itemprop="priceCurrency" content="UAH"><span itemprop="price" content="3249.00">3249</span> ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link open-popup" data-rel="card-preview">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite active"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-1.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge top">Top</div>
                            </div>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new black">3249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-main">
                            <a href="" class="add-favorite"></a>
                            <div class="product-images">
                                <a href="" class="active" data-src="img/products/prod-2.jpg"></a>
                                <a href="" data-src="img/products/prod-1.jpg"></a>
                                <a href="" data-src="img/products/prod-3.jpg"></a>
                                <a href="" data-src="img/products/product-5.jpg"></a>
                            </div>
                            <a href="" class="image">
                                <img class="lazy" data-src="img/products/prod-2.jpg" alt="">
                            </a>
                            <div class="badges">
                                <div class="badge action">-20%</div>
                                <div class="badge top">Top</div>
                                <div class="badge new">New</div>
                                <div class="badge promo">Promo</div>
                            </div>
                            <a href="" class="btn-video">Є відео</a>
                        </div>
                        <div class="product-info">
                            <a href="" class="product-title">LE'BERDES черевики осінні</a>
                            <div class="product-row">
                                <div class="price">
                                    <span class="new">3249 ₴</span>
                                    <span class="old">4249 ₴</span>
                                </div>
                            </div>
                            <div class="product-hover">
                                <div class="sizes">
                                    <div class="sizes-title">Разміри:</div>
                                    <ul>
                                        <li><a href="">34</a></li>
                                        <li><a href="">35</a></li>
                                        <li><a href="">36</a></li>
                                        <li><a href="">37</a></li>
                                        <li><a href="">39</a></li>
                                    </ul>
                                </div>
                                <a href="" class="link">Предперегляд</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- PAGINATION -->
                <section class="list-show-options">
                    <a href="" class="load-more">Показати ще 35</a>
                    <? include '_pagination.php';?>

                </section>

                <div class="breadcrumbs only-mob">
                    <ul>
                        <li><a href="index.php">Головна</a></li>
                        <li><a href="categories.php">Жіноче взуття</a></li>
                        <li><span>LE'BERDES черевики осінні</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- store CARDS -->
        <section>
            <? include '_features.php';?>
        </section>

        <!-- store CARDS -->
        <section>
            <? include '_store_cards.php';?>
        </section>

        <div class="seo-block hidden-xs bg-1">
            <div class="container">
                <h2 class="h3">Купити взуття вигідно та по-сучасному в інтернет-магазині favorite shoes у Києві, Львові та інших містах України</h2>
                <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                    З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>

                <div class="seo-hidden">
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                    <p>Перші черевики були вигадані дуже давно з метою захисту ніг від холоду, вологи та поранень.
                        З того часу дуже багато змінилося. Крім захисної функції, вони навіть більшою мірою виконують естетичну, адже мають відповідати конкретному вбранню. З кожним днем інтернет-магазин взуття в Україні має все більше клієнтів, адже робити покупки онлайн зручно та вигідно.</p>
                </div>
                <div class="seo-arrow"><img src="img/seo-arrow.svg" alt=""></div>
            </div>
        </div>

    </div>


    <!-- SEO TAG -->
    <section class="bg-1">
        <? include '_seo_tag.php';?>
    </section>

    <!-- FAQ -->
    <section class="bg-1">
        <? include '_FAQ.php';?>
    </section>

    <!-- SUBSCRIBE -->
    <section>
        <? include '_subscribe.php';?>
    </section>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>
    
    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>    

    <? include '_bottom.php';?>

</body>

</html>
