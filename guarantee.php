<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Гарантія</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>                        
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Гарантія</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="guarante-block">
                    <div class="guarante-row">
                        <div class="guarante-title h2 fw-500">
                            <img src="img/icon-guarante.svg" alt="" class="icon">
                            Гарантія на товар
                        </div>
                        <div class="text">Відповідно до закону України "Про захист прав споживачів" та нормативно-технічної документації, інтернет-магазин взуття та аксесуарів <b>Favorite Shoes</b> надає гарантійний термін експлуатації взуття протягом <b>30 днів</b> з дня продажу через роздрібну мережу (інтернет-магазин) або з початку сезону, за умови придбання взуття в несезонний період:</div>

                        <div class="guarante-season">
                            <div class="item">
                                <div class="title h2">Зимовий сезон
                                </div>
                                <div class="block-title type-2">з 15 Листопада до 15 Березня;</div>
                            </div>
                            <div class="item">
                                <div class="title h2">Літній
                                    сезон
                                </div>
                                <div class="block-title type-2">з 15 Листопада до 15 Березня;</div>
                            </div>
                            <div class="item">
                                <div class="title h2">Весняно —
                                    Осінній</div>
                                <div class="block-title type-2">з 15 Листопада до 15 Березня;</div>
                            </div>
                        </div>
                        <div class="text">Гарантійним терміном вважається період, протягом якого продавець бере на себе відповідальність заміни (ремонту) відповідної продукції за виявленням суттєвих дефектів , які виникли за виною виробника.
                            У випадку виявлення недоліків протягом гарантійного терміну, Вам необхідно вислати лист з фотодоказами браку (дефектів) на:</div>

                        <div class="guarante-contant">
                            <div class="item">
                                <div class="icon"><img src="img/guarantee/email.svg" alt=""></div>
                                <div class="block-title type-2">e-mail:</div>
                                <ul>
                                    <li>shop@favoriteshoes.com.ua</li>
                                </ul>
                            </div>
                            <div class="item">
                                <div class="icon"><img src="img/guarantee/viber.svg" alt=""></div>
                                <div class="block-title type-2">Viber</div>
                                <ul>
                                    <li>+38 063 178 12 27</li>
                                </ul>
                            </div>
                            <div class="item">
                                <div class="icon"><img src="img/guarantee/phone.svg" alt=""></div>
                                <div class="block-title type-2">Телефон:</div>
                                <ul>
                                    <li>+38 063 178 12 27</li>
                                    <li>+38 050 105 80 17</li>
                                </ul>
                            </div>
                        </div>
                        <div class="text">
                            За умови підтвердження дефекту по вині виробника, інтернет-магазин взуття і аксесуарів <b>Favorite Shoes</b> зобов'язується діяти згідно Статті 8 Закону України ) «Про захист прав споживачів».
                        </div>
                    </div>
                    <div class="guarante-row">
                        <div class="guarante-title h2 fw-500">
                            <img src="img/icon-not-guarante.svg" alt="" class="icon">
                            НЕ є гарантійним випадком:
                        </div>
                        <div class="ng-list">
                            <ul>
                                <li>Зношене взуття / з дефектами, які з'явилися внаслідок експлуатації 
в невідповідних умовах;</li>
                                <li>взуття з механічними пошкодженнями (порізи, пошкрябини, пропалене);</li>
                                <li>взуття, що було деформоване внаслідок неправильного догляду (сушіння, вплив хімічних засобів);</li>
                                <li>заломи шкіри в місцях найбільшого перегинання, які утворюються в процесі формування взуття по стопі, є наслідками експлуатації взуття та не є дефектом;</li>
                                <li>випране взуття не підлягає обміну та поверненню / уточнити необхідний догляд за обраною парою можна в нашому блозі або у консультантів;</li>
                                <li>випране взуття не підлягає обміну та поверненню / уточнити необхідний догляд за обраною парою можна в нашому блозі або у консультантів;</li>
                                <li>гарантія не розповсюджується на устілки, набойки, профілактики, елементи декору;</li>
                                <li>нерівномірність окрасу шкіри та фактури не є браком;</li>
                                <li>взуття з кольоровою підкладкою (чорного, червоного чи інших кольорів) або кольорове взуття без підкладки (наприклад босоніжки, сандалі), під дією вологи, поту та інших фізіологічних особливостей стопи може фарбувати ноги, шкарпетки, що не вважається дефектом виробництва / для уникнення цієї проблеми наші консультанти можуть запропонувати Вам придбати догляд, який закріплює колір;
відремонтоване покупцем взуття.</li> 
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
