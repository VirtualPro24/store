<div class="footer-top">
    <div class="spacer-75"></div>
    <div class="container">
        <div class="col-left">
            <a href="" class="logo">
                <img src="img/logo-w.svg" alt="">
            </a>
            <div class="social">
                <ul>
                    <li>
                        <a href=""><img src="img/Instagram.svg" alt=""></a>
                    </li>
                    <li>
                        <a href=""><img src="img/Facebook.svg" alt=""></a>
                    </li>                    
                </ul>
            </div>
            <div class="deliv-pay">
                <ul>
                    <li><img src="img/visa.svg" alt=""></li>
                    <li><img src="img/master.svg" alt=""></li>
                    <li><img src="img/nova-poshta.svg" alt=""></li>
                </ul>
            </div>
        </div>
        <div class="footer-nav">
            <div class="item">
                <div class="item-title">ПОКУПЦЮ</div>
                <ul>
                    <li><a href="">Гарантія</a></li>
                    <li><a href="">Відгуки </a></li>
                    <li><a href="">Оплата та доставка</a></li>
                    <li><a href="">Обмін та повернення</a></li>
                </ul>
            </div>
            <div class="item">
                <div class="item-title">ДОСТАВКА</div>
                <ul>
                    <li><a href="">Оплата та Доставка</a></li>
                    <li><a href="">Відділення Нової Почти</a></li> 
                </ul>
            </div>
            <div class="item">
                <div class="item-title">ІНШЕ</div>
                <ul>
                    <li><a href="">Знижки</a></li>
                    <li><a href="">Новини та акції</a></li>
                    <li><a href="">Мапа сайту</a></li>
                    <li><a href="">Дисконтна картка</a></li>
                    <li><a href="">Контакти</a></li>
                </ul>
            </div>                        
        </div>
        <div class="footer-contact">
            <a href="" class="phone">0 800 216 959</a>
            <div class="locationList">
                <ul>
                    <li>м. Львів, вул.Щирецька 36, 
ТК Південний, вул.Володимирська 2/20</li>
                   <li>м. Львів вул. Шпитальна, 1
ТЦ "Магнус" 2-й поверх пн-нд: 10:00-20:00</li>
                   <li>м. Львів вул. Виговського , 100
ТЦ "ВАМ" 2 поверх </li>
                </ul>
            </div>
        </div>
    </div>
</div>
 
<div class="footer-bottom">
    <div class="container">
        <div class="copy">Favorite Shoes © 2021. Всі права захищені</div>
        <ul>
            <li><a href="" class="link">Публічна оферта</a></li>
            <li><a href="" class="link">Політика конфіденційності</a></li>
        </ul>
    </div>
</div>