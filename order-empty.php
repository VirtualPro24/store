<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Мій профіль</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page settings-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Настройки</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="cabinet-nav">
                    <ul>
                        <li><a href="cabinet.php">Мій профіль</a></li>
                        <li><a href="order.php" class="active">Замовлення</a></li>
                        <li><a href="">Обране</a></li>
                    </ul>
                </div>

                <div class="order-empty">
                    <img src="img/order-empty.png" alt="" class="image">

                    <div class="text-15">У вас ще не має покупок</div>
                    <a href="" class="btn btn-stroke">перейти в магазину</a>
                </div>

       

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>
</html>