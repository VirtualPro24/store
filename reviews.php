<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Відгуки та запитання</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page review-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Відгуки</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>
                
                <div class="h1 page-title fw-500">Відгуки та запитання</div>

                <div class="reviews-block">
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/preview-product.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-1.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/preview-product.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-1.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/preview-product.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/other-prod-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-1.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="review-item">
                        <a href="" class="image">
                            <img src="img/insta/image-2.jpg" alt="">
                        </a>
                        <div class="review-content">
                            <a href="" class="review-title small-13">SASHA FABIANI ТУФЛІ</a>
                            <div class="date">24 Лютого 2020</div>
                            <p>Ношу второй сезон, удобные, тёплые, хороший замок. В 15 градусный мороз мне в них было тепло....</p>
                            <div class="review-bottom">
                                <div class="name">Влада</div>
                                <div class="rate">
                                    <img src="img/star.svg" alt="">
                                    <span>5.0</span>
                                </div>
                            </div>
                        </div>
                    </div>                                                                                                                       
                </div>
                <div class="text-center">
                    <a href="" class="btn btn-stroke">ще 36 відгуків</a>
                </div>

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>

</html>
