<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Кошик</title>
</head>

<body class="home-page bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="header">
            <? include '_header-order.php';?>
        </header>


        <div class="container">

            <div class="page-header">
                <a href="categories.php" class="link-back">Назад до магазину</a>
                <div class="h2 text-center fw-500">Ваш кошик</div>
            </div>

            <div class="cart">
                <div class="cart-table">
                    <div class="table-row table-head">
                        <div class="table-td fw-500 cart-td-info">Товар</div>
                        <div class="table-td fw-500 cart-td-size">Розмір</div>
                        <div class="table-td fw-500 cart-td-count">К-сть.</div>
                        <div class="table-td fw-500 cart-td-price">Ціна</div>
                    </div>
                    <div class="table-row table-body" data-tr-product>
                        <div class="table-td cart-td-info">
                            <a href="" class="image">
                                <img src="img/cart/cart.jpg" alt="">
                            </a>
                            <div class="text">
                                <span class="article">Артикул:59120313021</span>
                                <a href="" class="cart-product-title fw-500">LE'BERDES Черевики зимові</a>
                            </div>
                        </div>
                        <div class="table-td cart-td-size">
                            <div class="table-td-title">Розмір</div>
                            <div class="select-box select-box-line type-2">
                                <select class="SelectBox" name="">
                                    <option value="" selected>35</option>
                                    <option value="">36</option>
                                    <option value="">37</option>
                                    <option value="">38</option>
                                    <option value="">39</option>
                                    <option value="">40</option>
                                    <option value="">41</option>
                                    <option value="">42</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-td cart-td-count">
                            <div class="table-td-title">Кількість</div>
                            <div class="number-count input-wrap">
                                <div class="dec button"></div>
                                <input type="text" class="input" value="1" readonly>
                                <div class="inc button"></div>
                            </div>
                        </div>
                        <div class="table-td cart-td-price">
                            <div class="table-td-title">Сумма</div>
                            <div class="old">₴3789</div>
                            <div class="new">₴ 2789</div>
                        </div>
                        <div class="table-row-delete">
                            <img src="img/cart/delete.svg" alt="">
                        </div>
                        <div class="delete-popover" data-popover>
                            <div class="popover-content">
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="table-row table-body" data-tr-product>
                        <div class="table-td cart-td-info">
                            <a href="" class="image">
                                <img src="img/cart/cart.jpg" alt="">
                            </a>
                            <div class="text">
                                <span class="article">Артикул:59120313021</span>
                                <a href="" class="cart-product-title fw-500">LE'BERDES Черевики зимові</a>
                            </div>
                        </div>
                        <div class="table-td cart-td-size">
                            <div class="table-td-title">Розмір</div>
                            <div class="select-box select-box-line type-2">
                                <select class="SelectBox" name="">
                                    <option value="" selected>35</option>
                                    <option value="">36</option>
                                    <option value="">37</option>
                                    <option value="">38</option>
                                    <option value="">39</option>
                                    <option value="">40</option>
                                    <option value="">41</option>
                                    <option value="">42</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-td cart-td-count">
                            <div class="table-td-title">Кількість</div>
                            <div class="number-count input-wrap">
                                <div class="dec button"></div>
                                <input type="text" class="input" value="1" readonly>
                                <div class="inc button"></div>
                            </div>
                        </div>
                        <div class="table-td cart-td-price">
                            <div class="table-td-title">Сумма</div>
                            <div class="old">₴3789</div>
                            <div class="new">₴ 2789</div>
                        </div>
                        <div class="table-row-delete">
                            <img src="img/cart/delete.svg" alt="">
                        </div>
                        <div class="delete-popover" data-popover>
                            <div class="popover-content">
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="table-row table-body" data-tr-product>
                        <div class="table-td cart-td-info">
                            <a href="" class="image">
                                <img src="img/cart/cart.jpg" alt="">
                            </a>
                            <div class="text">
                                <span class="article">Артикул:59120313021</span>
                                <a href="" class="cart-product-title fw-500">LE'BERDES Черевики зимові</a>
                            </div>
                        </div>
                        <div class="table-td cart-td-size">
                            <div class="table-td-title">Розмір</div>
                            <div class="select-box select-box-line type-2">
                                <select class="SelectBox" name="">
                                    <option value="" selected>35</option>
                                    <option value="">36</option>
                                    <option value="">37</option>
                                    <option value="">38</option>
                                    <option value="">39</option>
                                    <option value="">40</option>
                                    <option value="">41</option>
                                    <option value="">42</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-td cart-td-count">
                            <div class="table-td-title">Кількість</div>
                            <div class="number-count input-wrap">
                                <div class="dec button"></div>
                                <input type="text" class="input" value="1" readonly>
                                <div class="inc button"></div>
                            </div>
                        </div>
                        <div class="table-td cart-td-price">
                            <div class="table-td-title">Сумма</div>
                            <div class="old">₴3789</div>
                            <div class="new">₴ 2789</div>
                        </div>
                        <div class="table-row-delete">
                            <img src="img/cart/delete.svg" alt="">
                        </div>
                        <div class="delete-popover" data-popover>
                            <div class="popover-content">
                                <p>Видалити товар з кошику?</p>
                                <ul class="delete-option">
                                    <li>
                                        <a href="" class="link" data-tr-remove>Видалити</a>
                                    </li>
                                    <li>
                                        <a href="" data-popover-close class="link">Відмінити</a>
                                    </li>
                                    <li>
                                        <a href="" class="link">До обраних</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="cart-bottom">
                    <a href="" class="btn btn-stroke type-2 btn-promo">В мене є промокод</a>
                    <div class="cart-total">
                        <p>
                            До сплати
                            <span class="total-price">9 967 ₴</span>
                        </p>
                        <a href="" class="btn btn-primary">Оформити замовлення</a>
                    </div>
                </div>
            </div>
            <div class="spacer-100 hidden-xs"></div>
        </div>

        <!-- store CARDS -->
        <section class="bg-2">
            <? include '_store_cards.php';?>
        </section>

    </div>



    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>



</body>

</html>
