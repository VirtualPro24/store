<!DOCTYPE html>
<html lang="en">

<head>
    <? include '_top.php';?>
    <title>Favorite Shoes | Мій профіль</title>
</head>

<body class="bg-1">

    <!-- LOADER -->
    <!--    <div id="loader"></div>-->

    <div id="content-block">

        <!-- HEADER -->
        <header class="site-header">
            <? include '_header.php';?>
        </header>
        <div class="margin-header"></div>


        <div class="inner-page settings-page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Головна</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="index.php" itemprop="item">Покупцю</a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <span itemprop="name">Настройки</span>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>

                <div class="cabinet-nav">
                    <ul>
                        <li><a href="cabinet.php" class="active">Мій профіль</a></li>
                        <li><a href="">Замовлення</a></li>
                        <li><a href="">Обране</a></li>
                    </ul>
                </div>

                <div class="profile-block">
                    <form action="">
                        <div class="pb-item">
                            <div class="pb-header">
                                <div class="block-title type-2">Мій профіль</div>
                                <a href="cabinet.php" class="link type-close">Відмінити</a>
                            </div>

                            <div class="pb-content">
                                <div class="input-wrap type-2 with-icon wrap-white value">
                                    <img src="img/icon-email.svg" alt="" class="icon">
                                    <label for="t1" class="input-title">E-mail</label>
                                    <input type="email" id="t1" class="input" value="ivanov@example.com">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white value">
                                    <img src="img/phone-brown.svg" alt="" class="icon">
                                    <label for="tt" class="input-title">Телефон</label>
                                    <input type="email" id="tt" class="input" value="+38 096 124 56 78">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white value">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t2" class="input-title">Iм’я</label>
                                    <input type="text" id="t2" class="input" value="Иван">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white value">
                                    <img src="img/user.svg" alt="" class="icon">
                                    <label for="t3" class="input-title">Прізвище</label>
                                    <input type="text" id="t3" class="input" value="Иванов">
                                </div>
                                <div class="input-wrap type-2 with-icon wrap-white value">
                                    <img src="img/Calendar.svg" alt="" class="icon">
                                    <label for="t5" class="input-title">Дата народження</label>
                                    <input type="text" id="t5" class="input date" value="01.01.2000">
                                </div>
                            </div>
                        </div>
 

                        <a href="cabinet.php" class="btn btn-stroke">Зберегти </a>
                    </form>
                </div>

            </div>
        </div>


    </div>

    <!-- FOOTER -->
    <footer class="footer">
        <? include '_footer.php';?>
    </footer>

    <!-- POPUP -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>
        <? include '_popup.php';?>
    </div>

    <? include '_bottom.php';?>

</body>
</html>